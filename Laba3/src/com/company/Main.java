package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    private static void menu() {
        System.out.println("\n               МЕНЮ ");
        System.out.println(" 1 - Вывод");
        System.out.println(" 2 - Добавление");
        System.out.println(" 3 - Удаление");
        System.out.println(" 4 - Обновление");
        System.out.println(" 5 - Вывод счетчика продуктов");
        System.out.println(" 0 - Выход");
    }

    public static void main(String[] args) {



        int var = 1;
        Scanner in = new Scanner(System.in);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Orders orders = new Orders();

        while (var != 0) {
            menu();

            System.out.println(">>");


            var = in.nextInt();

            switch (var) {

                case 0: {
                    break;
                }

                /////////////////////////////ЧТЕНИЕ/////////////////////////////////////////////////////////////////////
                case 1: {
                    orders.read();
                    break;
                }

                /////////////////////////////ДОБАВЛЕНИЕ/////////////////////////////////////////////////////////////////
                case 2: {
                    orders.create();
                    break;
                }
                /////////////////////////////УДАЛЕНИЕ///////////////////////////////////////////////////////////////////
                case 3: {
                    orders.delete();
                    break;
                }

                /////////////////////////////ОБНОВЛЕНИЕ/////////////////////////////////////////////////////////////////
                case 4: {
                    orders.check_orders();
                    break;
                }

                /////////////////////////////ВЫВОД СЧЕТЧИКА/////////////////////////////////////////////////////////////
                case 5: {

                    System.out.println(" \n Текущее число объектов : " + Product.counter);
                    break;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }
            }
        }


        in.close();
    }


}
