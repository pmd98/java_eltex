package com.company.ACheck;

import com.company.Orders.Orders;
import com.company.Orders.Order;

public class ACheckTrue<T extends Order> extends ACheck {

    public ACheckTrue() {
        super();
    }

    public ACheckTrue(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
        System.out.println("\nACheckTrue\n");

       // synchronized (orders) {
            for (int i = orders.size() - 1; i >= 0; --i) {
                if (orders.get(i).getStatus()) {
                    orders.get(i).delete();
                    orders.remove(i);
                }
            }
       // }

    }
}
