package com.company.Orders;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.UUID;

public class ShoppingCart<T extends Product> implements ICrudAction {

    public ShoppingCart() {
        list = new ArrayList<T>();
        listID = new HashSet<UUID>();
    }

    public boolean add(T data) {

        boolean check = listID.add(data.getProductID());

        if (check) {
            list.add(data);
            return true;
        } else {
            return false;
        }
    }

    public Product delete(int index) {
        Product data;
        data = list.remove(index);
        return data;
    }

    public Product get(int index) {
        return list.get(index);
    }

    @Override
    public void create() {

        int random_number = (int) (Math.random() * 10 % 2 + 3);

        for (int i = 0; i < random_number; ++i) {
            T coffe = (T) new Coffe();
            coffe.create();
            T tea = (T) new Tea();

            //Tea tea = new Tea();
            tea.create();
            list.add(coffe);
            list.add(tea);
            listID.add(coffe.getProductID());
            listID.add(tea.getProductID());
        }
    }

    @Override
    public void update() {
        Scanner vybor = new Scanner(System.in);

        System.out.println("Ввод продуктов в корзине");
        System.out.println("Ввод количества продукта [кофе] в корзине:");
        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);


        for (int i = 0; i < b; ++i) {
            T coffe = (T) new Coffe();
            System.out.println("\nВвод данных в объект №" + (i + 1) + ":");
            coffe.update();
            list.add(coffe);
            listID.add(coffe.getProductID());
        }
        System.out.println("Ввод количества продукта [чай] в корзине:");

        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);


        for (int i = 0; i < b; ++i) {
            T tea = (T) new Tea();
            System.out.println("\nВвод данных в объект №" + (i + 1) + ":");
            tea.update();
            list.add(tea);
            listID.add(tea.getProductID());
        }
    }

    @Override
    public void delete() {

        for (int i = 0; i < list.size(); ++i) {
            list.get(i).delete();
        }
        list.clear();
        listID.clear();
    }

    @Override
    public void read() {

        for (int i = 0; i < list.size(); ++i) {
            list.get(i).read();
            System.out.println();
        }
    }

    public long size() {
        return list.size();
    }

    public boolean search_by_ID(UUID id) {
        return listID.contains(id);
    }


    public void setList(ArrayList<T> list) {
        this.list = list;
    }

    public ArrayList<T> getList() {
        return list;
    }

    private ArrayList<T> list;
    private HashSet<UUID> listID;


}