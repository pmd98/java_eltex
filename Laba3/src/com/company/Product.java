package com.company;

import com.company.ICrudAction;

import java.util.UUID;

public abstract class Product implements ICrudAction {

    Product() {
        this.productID = null;
        this.company_supplier = null;
        this.country_manufacturer = null;
        this.cost = 0;
        this.name = null;
    }

    @Override
    public abstract void create(); //  заполнение объекта случайными значениями и инкремент счётчика.

    @Override
    public abstract void update(); //  ввод данных с клавиатуры

    @Override
    public abstract void delete(); //  принудительное зануление данных в объекте и декремент счетчика

    @Override
    public abstract void read(); //    вывод данных на экран.

    public UUID getProductID() {
        return productID;
    }

    protected UUID productID;               //  ID товара
    protected double cost;                 //  Цена
    protected String name;                  //  Название
    protected String company_supplier;      //  Фирма поставщик
    protected String country_manufacturer;  //  Страна производитель

    protected static int counter;           //  Счётчик товаров


}