package com.company.Network;
import com.company.Orders.Order;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class Client {
    private static final int SIZE_OF_INT = 4;
    private static final int TIMEOUT = 2;
    private InetAddress host;
    private int portTCP;
    private int portUDP;
    private Order order;

    public Client() {
        portUDP = 4444;
        order = new Order();
    }

    public Client(int portUDP) {
        this.portUDP = portUDP;
        order = new Order();
    }

    public void getPort() {
        System.out.println("Ожидание TCP порта");
        while (true) {
            try {
                DatagramPacket packet = new DatagramPacket(new byte[SIZE_OF_INT], SIZE_OF_INT);
                DatagramSocket socket = new DatagramSocket(portUDP);
                socket.receive(packet);
                byte[] data = packet.getData();
                portTCP = ByteBuffer.wrap(data).getInt();
                host = packet.getAddress();
                socket.close();
                if (portTCP >= 1024) {
                    System.out.println("Порт получен");
                    break;
                }
                TimeUnit.SECONDS.sleep(TIMEOUT);
            } catch (BindException ex) {
                System.err.println("Порт " + portUDP + "занят. Пробую следующий...");
                portUDP++;
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void sentData() {
        System.out.println("Подключение к серверу...");
        try (Socket client = new Socket(host, portTCP)) {
            System.out.println("Успешно подключен");
            order.create();
            order.read();
            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            out.writeInt(portUDP);
            out.writeObject(order);
           // TimeUnit.SECONDS.sleep(TIMEOUT);

        } catch (IOException e) {
            e.printStackTrace();
        } //catch (InterruptedException e) {
         //   e.printStackTrace();
      //  }
        System.out.println("Заказ оправлен");
    }

    public void getMessage() {
        try {
            System.out.println("Ожидание ответа...");
            int status=0;
            while (status!=1) {
                DatagramPacket packet = new DatagramPacket(new byte[SIZE_OF_INT], SIZE_OF_INT);
                DatagramSocket socket = new DatagramSocket(portUDP);
                socket.receive(packet);
                byte[] data = packet.getData();
                status = ByteBuffer.wrap(data).getInt();
                socket.close();
                if (1 == status) {
                    order.setOrderStatus(true);
                    order.read();
                    System.out.println("\nВремя обработки: " + order.getTimeOfWaiting().toString() + "\n");
                }
            }
           // TimeUnit.SECONDS.sleep(TIMEOUT);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } //catch (InterruptedException e) {
          //  e.printStackTrace();
       // }
    }

    public static void main(String[] args) {
        Client client = new Client(4445);
        while (true) {
            client.getPort();
            client.sentData();
            client.getMessage();
            try {
                TimeUnit.SECONDS.sleep(TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}