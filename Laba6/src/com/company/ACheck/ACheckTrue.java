package com.company.ACheck;

import com.company.Network.Server;
import com.company.Orders.Orders;
import com.company.Orders.Order;
import com.company.Network.Server.*;

import java.util.Iterator;

public class ACheckTrue<T extends Order> extends ACheck {

    public ACheckTrue() {
        super();
    }

    public ACheckTrue(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
        synchronized (orders) {

            for (Iterator<Order> iter = orders.iterator(); iter.hasNext(); ) {

                Order order = iter.next();
                if (order.getSentStatus()) {
                    order.read();
                    System.err.println("\nwas deleted");
                    iter.remove();


                }
            }
        }

    }
}
