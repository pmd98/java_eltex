package com.company.Deserializer;

import com.company.Orders.Credentials;
import com.company.Orders.Order;
import com.company.Orders.ShoppingCart;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class OrderDeserializer implements JsonDeserializer<Order> {
    @Override
    public Order deserialize(JsonElement json, Type typeOfSrc, JsonDeserializationContext context)
            throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        Order order = new Order();

        order.setTimeOfCreature(jsonObject.get("timeOfCreature").getAsLong());
        order.setTimeOfWaiting(jsonObject.get("Время ожидания").getAsLong());
        order.setOrderStatus(jsonObject.get("Статус заказа").getAsString());
        order.setCredentials((Credentials) context.deserialize(jsonObject.get("Данные пользователя"), Credentials.class));
        order.setShoppingCart((ShoppingCart) context.deserialize(jsonObject.get("Корзина"), ShoppingCart.class));
        return order;
    }
}