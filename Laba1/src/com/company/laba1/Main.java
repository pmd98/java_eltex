package com.company.laba1;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static void choose_product() {
        System.out.println("\n==============  Выбор Объекта  =============");
        System.out.println(" 1 - Чай");
        System.out.println(" 2 - Кофе");
        System.out.println(" 3 - Вывод счетчика объектов");
        System.out.println(" 0 - Выход");
    }

    private static void menu() {
        System.out.println("\n==============  МЕНЮ  ================");
        System.out.println(" 1 - Вывод");
        System.out.println(" 2 - Добавление");
        System.out.println(" 3 - Удаление");
        System.out.println(" 4 - Обновление");
        System.out.println(" 0 - Выход");

    }

    private static int vybor_do_a(int a) {
        Scanner vybor_do_a = new Scanner(System.in);

        int b;
        do {
            b = vybor_do_a.nextInt();
            if ((b >= 0) && (b <= a)) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);

       // vybor_do_a.close();
        return b;
    }

    private static int vybor() {
        Scanner vybor = new Scanner(System.in);

        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);
       // vybor.close();
        return b;

    }


    public static void main(String[] args) {
        // write your code here
        // System.out.println("Hello ");

        final String _H = "Н";
        final String _h = "н";
        final String _D = "Д";
        final String _d = "д";

        final String _N = "N";
        final String _n = "n";
        final String _Y = "Y";
        final String _y = "y";

        int var = 1;
        int chose = 1;
        String question;

        Scanner in = new Scanner(System.in);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int N;
        System.out.println("Введите количество объектов [чай]: ");
        do {

            N = in.nextInt();

            if (N >= 0)
                break;
            System.out.println(" Некорректный ввод! ");
        } while (true);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int M;
        System.out.println("Введите количество объектов [кофе]: ");
        do {

            M = in.nextInt();

            if (M >= 0)
                break;
            System.out.println(" Некорректный ввод! ");
        } while (true);


        ArrayList<Tea> teas = new ArrayList<Tea>(N);

        ArrayList<Coffe> coffes = new ArrayList<Coffe>(M);

        // Tea[] teas = new Tea[N];

        //Coffe[] coffes = new Coffe[M];

        Scanner str = new Scanner(System.in);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do {




            System.out.println(" Заполнить " + teas.size() + " объектов [чай] случайными значениями? [Д/Н]");

            question=null;
            question = str.nextLine();


            if (question.equals(_d) || question.equals(_D)|| question.equals(_Y)|| question.equals(_y)) {


                for (int i = 0; i <  N; i++) {
                    Tea tea = new Tea();
                    tea.create();
                    teas.add(tea);
                }
                break;
            }

            else if (question.equals(_N) || question.equals(_n) || question.equals(_H)  || question.equals(_h) ){


                for (int i = 0; i < N; i++) {
                    if (i == 0)
                        System.out.println("\t\tВВОД ДАННЫХ [ЧАЙ]:\n");
                    System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                    Tea tea = new Tea();
                    tea.update();
                    teas.add(tea);
                }
                break;
            }

            else {
                System.out.println(" Некорректный ВВод!");
            }
        }
        while (true);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do {

            System.out.println(" Заполнить " + coffes.size() + " объектов [кофе] случайными значениями? [Д/Н]");

            question=null;
            question = str.nextLine();


            if (question.equals(_d) || question.equals(_D)|| question.equals(_Y)|| question.equals(_y)) {

                for (int i = 0; i <  M; i++) {

                    Coffe coffe = new Coffe();
                    coffe.create();
                    coffes.add(coffe);
                }
                break;
            }

            else if (question.equals(_N) || question.equals(_n)|| question.equals(_H)|| question.equals(_h)) {


                for (int i = 0; i < M; i++) {
                    if (i == 0)
                        System.out.println("\t\tВВОД ДАННЫХ [КОФЕ]:\n");
                    System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                    Coffe coffe = new Coffe();
                    coffe.update();
                    coffes.add(coffe);
                }
                break;

            }

            else {
                System.out.println(" Некорректный ВВод!");
            }
        }
        while (true);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        while (chose != 0) {
            var = 1;
            choose_product();

            System.out.println(">>");

            chose = in.nextInt();

            switch (chose) {
                case 1: {


                    while (var != 0) {
                        menu();

                        System.out.println(">>");


                        var = in.nextInt();

                        switch (var) {
                            case 0: {
                                break;
                            }
                            /////////////////////////////ЧТЕНИЕ////////////////////////////////////////////////////////
                            case 1: {
                                for (int i = 0; i < teas.size(); i++) {

                                    System.out.println("\n Объект [чай] номер "+(i+1)+" :");
                                    teas.get(i).read();

                                }
                                break;
                            }
                            /////////////////////////////ДОБАВЛЕНИЕ////////////////////////////////////////////////////
                            case 2: {

                                System.out.println(" Введите количество объектов для добавления (0 для выхода): ");
                                int a = vybor();

                                if (a == 0)
                                    break;

                                ///////////////////////////////////////////////////////////////
                                do {

                                    System.out.println(" Заполнить " + a + " объектов [чай] случайными значениями? [Д/Н]");

                                    question=null;
                                    question = str.nextLine();


                                    if (question.equals(_d) || question.equals(_D)|| question.equals(_Y)|| question.equals(_y)) {


                                        for (int i = 0; i <  a; i++) {
                                            Tea tea = new Tea();
                                            tea.create();
                                            teas.add(tea);
                                        }
                                        break;

                                    }

                                    else if (question.equals(_H) || question.equals(_h)|| question.equals(_N)|| question.equals(_n)) {

                                        for (int i = 0; i < a; i++) {
                                            if (i == 0)
                                                System.out.println("\t\tВВОД ДАННЫХ [ЧАЙ]:\n");
                                            System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                                            Tea tea = new Tea();
                                            tea.update();
                                            teas.add(tea);
                                        }
                                        break;

                                    }

                                    else {
                                        System.out.println(" Некорректный ВВод!");
                                    }
                                }
                                while (true);
                                ////////////////////////////////////////////////////////////////////////

                                System.out.println(" Добавлено " + a + " объектов");
                                break;
                            }
                            /////////////////////////////////УДАЛЕНИЕ//////////////////////////////////////////////////
                            case 3: {

                                System.out.println(" Введите количетсво элементов для удаления (0 для выхода): ");

                                int a = vybor_do_a(teas.size());

                                if (a == 0)
                                    break;

                                int[] A = new int[a];

                                System.out.println(" Введите номера элементов для удалления: ");

                                for (int i = 0; i < a; i++) {
                                    A[i] = vybor_do_a(teas.size()) - 1;
                                }

                                for (int i = 0 ; i < a; i++) {

                                    teas.get(A[i]).delete();
                                    teas.remove(A[i]);
                                }

                                System.out.println(" Удалено " + a + " объектов");

                                break;
                            }

                            //////////////////////////////////////ОБНОВЛЕНИЕ///////////////////////////////////////////
                            case 4: {

                                System.out.println(" Введите номер объектов для обновления (0 для выхода): ");
                                int a = vybor_do_a(teas.size()) - 1;
                                if (a == -1)
                                    break;
                                teas.get(a).update();
                                System.out.println(" Изменен " + (a + 1) + " объект");
                                break;
                            }




                            default: {
                                System.out.println(" Введено неверное число\nПовторите ввод: ");
                                break;
                            }
                        }
                    }


                    break;
                }
                case 2: {


                    while (var != 0) {
                        menu();

                        System.out.println(">>");


                        var = in.nextInt();

                        switch (var) {
                            case 0: {
                                break;
                            }
                            /////////////////////////////ЧТЕНИЕ////////////////////////////////////////////////////////
                            case 1: {
                                for (int i = 0; i < coffes.size(); i++) {

                                    System.out.println("\n Объект [кофе] номер "+(i+1)+" :");
                                    coffes.get(i).read();

                                }
                                break;
                            }
                            /////////////////////////////ДОБАВЛЕНИЕ////////////////////////////////////////////////////
                            case 2: {

                                System.out.println(" Введите количество объектов для добавления (0 для выхода): ");
                                int a = vybor();

                                if (a == 0)
                                    break;

                                //////////////////////////////////////////////////////////////////////////////////
                                do {

                                    System.out.println(" Заполнить " + a + " объектов [кофе] случайными значениями? [Д/Н]");

                                    question=null;
                                    question = str.nextLine();


                                    if (question.equals(_d) || question.equals(_D)|| question.equals(_Y)|| question.equals(_y)) {

                                        for (int i = 0; i <  a; i++) {

                                            Coffe coffe = new Coffe();
                                            coffe.create();
                                            coffes.add(coffe);
                                        }
                                        break;
                                    }

                                    else if (question.equals(_N) || question.equals(_n)|| question.equals(_H)|| question.equals(_h)) {

                                        for (int i = 0; i < a; i++) {
                                            if (i == 0)
                                                System.out.println("\t\tВВОД ДАННЫХ [КОФЕ]:\n");
                                            System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                                            Coffe coffe = new Coffe();
                                            coffe.update();
                                            coffes.add(coffe);
                                        }
                                        break;
                                    }

                                    else {
                                        System.out.println(" Некорректный ВВод!");
                                    }
                                }
                                while (true);
                                //////////////////////////////////////////////////////////////////////////////////////


                                System.out.println(" Добавлено " + a + " объектов");
                                break;
                            }
                            /////////////////////////////////УДАЛЕНИЕ//////////////////////////////////////////////////
                            case 3: {

                                System.out.println(" Введите количетсво элементов для удаления (0 для выхода): ");

                                int a = vybor_do_a(coffes.size());

                                if (a == 0)
                                    break;

                                Coffe[] bufer = new Coffe[coffes.size() - a];
                                int[] A = new int[a];

                                System.out.println(" Введите номера элементов для удалления: ");

                                for (int i = 0; i < a; i++) {
                                    A[i] = vybor_do_a(coffes.size()) - 1;
                                }

                                for (int i = 0 ; i < a; i++) {

                                     coffes.get(A[i]).delete();
                                     coffes.remove(A[i]);
                                }

                                System.out.println(" Удалено " + a + " объектов");

                                break;
                            }
                            //////////////////////////////////////ОБНОВЛЕНИЕ///////////////////////////////////////////
                            case 4: {

                                System.out.println(" Введите номер объектов для обновления : ");
                                int a = vybor_do_a(coffes.size()) - 1;
                                if (a == -1)
                                    break;
                                coffes.get(a).update();
                                System.out.println(" Изменен " + (a + 1) + " объект");
                                break;
                            }




                            default: {
                                System.out.println(" Введено неверное число\nПовторите ввод: ");
                                break;
                            }
                        }
                    }


                    break;
                }

                //////////////////////////////////////ВЫВОД СЧЕТЧИКА////////////////////////////////////////
                case 3: {

                    System.out.println(" \n Текущее число объектов : "+Product.counter);
                    break;
                }

                case 0: {
                    break;
                }
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }

            }
        }

        str.close();
        in.close();
    }


}
