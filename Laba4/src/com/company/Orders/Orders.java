package com.company.Orders;

import java.util.*;

public class Orders<T extends Order> implements ICrudAction {

    public Orders() {
        this.orders = new LinkedList<T>();
        this.order_hash = new HashMap<Date, T>();
    }

    @Override
    public void read() {
       // synchronized (orders) {
            if (orders.isEmpty()) {
                System.out.println("Список заказов пуст!");
                return;
            }

            for (int i = 0; i < orders.size(); i++) {
                System.out.println("\nЗаказ №" + (i + 1) + ":\n");
                this.orders.get(i).read();
            }
       // }
    }

    @Override
    public void create() {
       // synchronized (orders) {
            int random_number = (int) (Math.random() * 10 % 2 + 1);

            for (int i = 0; i < random_number; ++i) {
                T order = (T) new Order();
                order.create();
                orders.add(order);

            }
       // }
    }

    @Override
    public void update() {
        Scanner vybor = new Scanner(System.in);

        System.out.println("Ввод количество заказов:");
        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);

        for (int i = 0; i < b; ++i) {
            T order = (T) new Order();
            System.out.println("Ввод данных в заказ №" + (i + 1) + ": ");
            order.update();
            orders.add(order);
            order_hash.put(order.getTimeOfCreature(), order);
        }
    }

    @Override
    public void delete() {
        orders.clear();
        order_hash.clear();
    }

    public void issuePurchase(Credentials credential, ShoppingCart shoppingCart) {
        T order = (T) new Order(shoppingCart, credential);
        orders.push(order);
        order_hash.put(order.getTimeOfCreature(), order);
    }

    public void checkOrders() {
        for (int i = orders.size() - 1; i >= 0; --i) {
            if (orders.get(i).is_complete()) {

                order_hash.remove(orders.get(i).getTimeOfCreature());
                orders.get(i).delete();
                orders.remove(i);

            }
        }
    }

    public T get(int index) {
        return orders.get(index);
    }

    public int size() {
        return orders.size();
    }

    public T remove(int index) {
        return orders.remove(index);
    }

    public boolean add(T order) {
        boolean ch = order_hash.containsKey(order.getTimeOfCreature());
        if (ch)
            return false;
        order_hash.put(order.getTimeOfCreature(), order);
        orders.push(order);
        return true;
    }


    private volatile LinkedList<T> orders;
    private HashMap<Date, T> order_hash;
}