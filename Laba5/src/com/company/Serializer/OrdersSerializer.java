package com.company.Serializer;

import com.company.Orders.Orders;
import com.google.gson.*;

import java.lang.reflect.Type;

public class OrdersSerializer implements JsonSerializer<Orders> {
    @Override
    public JsonElement serialize(Orders src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject result = new JsonObject();
        int i = 1;
        for (Object orders : src.getOrders()) {
            StringBuilder builder = new StringBuilder("Заказ №");
            builder.append(i);
            result.add(builder.toString(), context.serialize(orders));
            ++i;
        }
        return result;
    }
}
