package com.company.Orders;
import java.util.*;

public class Orders<T extends Order> implements ICrudAction {

    public Orders() {
        this.orders =  Collections.synchronizedList(new LinkedList<T>());
        //this.orders = new LinkedList<T>();
        this.order_hash = new HashMap<Date, T>();
    }

    @Override
    public void read() {
        synchronized (orders) {
            if (orders.isEmpty()) {
                System.out.println("Список заказов пуст!");
                return;
            }

            int i=1;
            for ( Iterator<T> iter = orders.iterator(); iter.hasNext();i++ ){
                System.out.println("\nЗаказ №" +i+ ":\n");
                Order order =iter.next();
                order.read();
            }

       }
    }

    @Override
    public void create() {
        synchronized (orders) {
            int random_number = (int) (Math.random() * 10 % 5 + 1);

            for (int i = 0; i < random_number; ++i) {
                T order = (T) new Order();
                order.create();

                orders.add(order);

            }
      }
    }

    @Override
    public void update() {
        Scanner vybor = new Scanner(System.in);

        System.out.println("Ввод количество заказов:");
        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);

        for (int i = 0; i < b; ++i) {
            T order = (T) new Order();
            System.out.println("Ввод данных в заказ №" + (i + 1) + ": ");
            order.update();
            orders.add(order);
            order_hash.put(order.getTimeOfCreature(), order);
        }
    }

    @Override
    public void delete() {
        orders.clear();
        order_hash.clear();
    }

    public void issuePurchase(Credentials credential, ShoppingCart shoppingCart) {
        T order = (T) new Order(shoppingCart, credential);
        orders.add(order);
        order_hash.put(order.getTimeOfCreature(), order);
    }

    public void checkOrders() {
        for (int i = orders.size() - 1; i >= 0; --i) {
            if (orders.get(i).isComplete()) {

                order_hash.remove(orders.get(i).getTimeOfCreature());
                orders.get(i).delete();
                orders.remove(i);

            }
        }
    }

    public T get(int index) {
        return orders.get(index);
    }

    public int size() {
        return orders.size();
    }

    public T remove(int index) {
        return orders.remove(index);
    }

    public boolean add(T order) {
      //  boolean ch = order_hash.containsKey(order.getTimeOfCreature());
      //  if (ch)
      //      return false;
      //  order_hash.put(order.getTimeOfCreature(), order);
        orders.add(order);
        return true;
    }

    public List<T> getOrders() {
        return orders;
    }

    public Iterator<T> iterator() {
        return orders.iterator();
    }

    public void setOrders(LinkedList<T> orders) {
        this.orders = orders;
    }

    //private volatile LinkedList<T> orders;
    private HashMap<Date, T> order_hash;
    private List<T> orders ;//=  Collections.synchronizedList(new LinkedList<T>());


}