package com.company.ACheck;

import com.company.Orders.Order;
import com.company.Orders.Orders;

public abstract class ACheck<T extends Order> implements Runnable {

    public ACheck() {
        orders = new Orders<>();
        thread = new Thread(this);
        thread.start();
    }

    public ACheck(Orders<T> orders) {
        this.orders = orders;
        thread = new Thread(this);
        thread.start();
    }


    public Thread getThread(){
       return thread;
    }

    @Override
    public void run() {
       // System.out.println("\nAcheck\n");
    }

    protected Thread thread;
    protected volatile Orders<T> orders;
}