
package com.company.Network;

import com.company.ACheck.ACheck;
import com.company.ACheck.ACheckFalse;
import com.company.ACheck.ACheckTrue;
import com.company.Orders.Order;
import com.company.Orders.Orders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Server {


    private static final int TCP_PORT_MIN = 4444;
    private static final int TCP_PORT_MAX = 4454;
    private static final int SIZE_OF_INT = 4;
    private static final int TIMEOUT = 2;
    private static final int SERVER_TCP_PORT = 5555;
    private volatile InetAddress host;
    private volatile InetAddress broadcast;

    private volatile Orders<Order> orders;
    private volatile List<Integer> ports;


    public static void main(String[] args) throws UnknownHostException {
        System.out.println("Сервер запущен...");
        Server server = new Server();
        server.run();
    }

    public Server() {
        try {
            byte[] localhost = {new Integer(192).byteValue(), new Integer(168).byteValue(), 0, 112};
            host = InetAddress.getByAddress(localhost);
            System.err.println("\nLOCALHOST: " + host.toString());

            byte[] broadcast = {new Integer(192).byteValue(), new Integer(168).byteValue(), 0, new Integer(255).byteValue()};
            this.broadcast = InetAddress.getByAddress(broadcast);
            System.err.println("\n BROADCAST: " + this.broadcast.toString());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        ports = new ArrayList<>();
        orders = new Orders<>();
        for (int i = TCP_PORT_MIN; i < TCP_PORT_MAX; i++) {
            ports.add(i);
        }
    }


    class PortSender implements Runnable {
        @Override
        public void run() {

            synchronized (ports) {
                while (true) {
                    try {

                        System.out.println("Рассылка портов");
                        byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(SERVER_TCP_PORT).array();
                        for (int i = 0; i < ports.size(); i++) {
                            DatagramPacket packet = new DatagramPacket(data, data.length, broadcast, ports.get(i).intValue());
                            DatagramSocket socket = new DatagramSocket();
                            socket.send(packet);
                            socket.close();

                        }

                        TimeUnit.SECONDS.sleep(TIMEOUT);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    } catch (SocketException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    class SenderStatus {
        public void sentStatus(int index) {
            try {
                int status = 1;
                byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(status).array();
                System.out.println("Заказ " + orders.get(index).getCredentialsID() + " обработан");
                System.out.println("IP: " + orders.get(index).getInetAddress().toString() + "   Port: " + orders.get(index).getPort());
                DatagramPacket packet = new DatagramPacket(data, data.length, orders.get(index).getInetAddress(), orders.get(index).getPort());
                DatagramSocket socket = new DatagramSocket();
                socket.send(packet);
                socket.close();
                orders.get(index).setSentStatus(true);
                //ports.add(orders.get(index).getPort());
                TimeUnit.SECONDS.sleep(TIMEOUT);
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void sentStatus(int index) {
        try {
            int status = 1;
            byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(status).array();
            System.out.println("Заказ " + orders.get(index).getCredentialsID() + " обработан");
            System.out.println("IP: " + orders.get(index).getInetAddress().toString() + "   Порт: " + orders.get(index).getPort());
            DatagramPacket packet = new DatagramPacket(data, data.length, orders.get(index).getInetAddress(), orders.get(index).getPort());
            DatagramSocket socket = new DatagramSocket();
            socket.send(packet);
            socket.close();
            orders.get(index).setSentStatus(true);
            //ports.add(orders.get(index).getPort());
            TimeUnit.SECONDS.sleep(TIMEOUT);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public void run() {
        Thread threadPort = new Thread(new PortSender());
        Thread threadData = new Thread(new GetData());
        Thread threadProcessing = new Thread(this::processing);

        threadPort.start();
        threadData.start();
        threadProcessing.start();
    }


    public void processing() {
        while (true) {
            ACheck statusFalse;
            ACheck statusTrue;

            try {
                statusFalse = new ACheckFalse(orders);
                statusFalse.getThread().join();


                for (int i = 0; i < orders.size(); i++) {
                    if (orders.get(i).getStatus()) {
                        sentStatus(i);
                        System.out.println(" === " + i + " === ");
                    }
                }
                    /*for (Iterator iter =orders.getOrders().iterator();iter.hasNext();i++) {
                        Order order= (Order) iter.next();
                        if (order.getStatus()) {
                            sentStatus(i);
                            System.out.println(" === " + i + " === ");
                        }
                    }*/


                statusTrue = new ACheckTrue(orders);
                statusTrue.getThread().join();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    class GetData implements Runnable {

        public void run() {

            try (ServerSocket serverSocket = new ServerSocket(SERVER_TCP_PORT)) {
                while (true) {
                    System.out.println("Ожидание подключения клиента...");
                    Socket clientSocket = serverSocket.accept();
                    System.out.println("Клиент подключен");
                    new Thread(new ClientHandler(clientSocket)).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    class ClientHandler implements Runnable {

        Socket clientSocket;

        ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;

        }

        public void run() {
            try (ObjectInputStream in = (new ObjectInputStream(clientSocket.getInputStream()))) {
                int port = in.readInt();
                Order order = (Order) in.readObject();
                order.setPort(port);
                order.setInetAddress(clientSocket.getInetAddress());
                synchronized (orders) {
                    order.read();
                    orders.add(order);
                    in.close();
                    clientSocket.close();
                    TimeUnit.SECONDS.sleep(Server.TIMEOUT);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* public void sendPort() {

        synchronized (ports) {
            while (true) {
                try {

                    System.out.println("Рассылка портов");
                    byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(SERVER_TCP_PORT).array();
                    for (int i = 0; i < ports.size(); i++) {
                        DatagramPacket packet = new DatagramPacket(data, data.length, broadcast, ports.get(i).intValue());
                        DatagramSocket socket = new DatagramSocket();
                        socket.send(packet);
                        socket.close();

                    }

                    TimeUnit.SECONDS.sleep(TIMEOUT);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/




   /* public void getData() {

        while (true) {
            System.out.println("Ожидание подключения клиента...");
            try (ServerSocket server = new ServerSocket(SERVER_TCP_PORT)) {

                Socket client = server.accept();
                System.out.println("Клиент подключен");

                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                int port = in.readInt();
                //ports.remove((Integer) port);
                Order order = (Order) in.readObject();
                order.setPort(port);
                order.setInetAddress(client.getInetAddress());
                order.read();
                orders.add(order);
                in.close();
                client.close();
                TimeUnit.SECONDS.sleep(TIMEOUT);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Заказ получен");
        }
    }*/


}


/*package com.company.Network;

import com.company.ACheck.ACheck;
import com.company.ACheck.ACheckFalse;
import com.company.ACheck.ACheckTrue;
import com.company.Orders.Order;
import com.company.Orders.Orders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Server {

    private static final int TCP_PORT_MIN = 4444;
    private static final int TCP_PORT_MAX = 4454;
    private static final int SIZE_OF_INT = 4;
    private static final int TIMEOUT = 2;
    private volatile InetAddress host;
    private volatile InetAddress broadcast;
    private volatile int portTCP;
    private volatile Orders<Order> orders;
    private volatile List<Integer> ports;
    private static final Semaphore SEMAPHORE = new Semaphore(1, true);





    public Server() {
        try {
            byte[] localhost = {new Integer(192).byteValue(), new Integer(168).byteValue(), 0, 112};
            host = InetAddress.getByAddress(localhost);
            System.err.println("\nLOCALHOST: " + host.toString());
            byte[] broadcast = {new Integer(192).byteValue(), new Integer(168).byteValue(), 0, new Integer(255).byteValue()};
            this.broadcast = InetAddress.getByAddress(broadcast);
            System.err.println("\n BROADCAST: " + this.broadcast.toString());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        portTCP = 5555;
        ports = new ArrayList<>();
        orders = new Orders<>();
        for (int i = TCP_PORT_MIN; i < TCP_PORT_MAX; i++) {
            ports.add(i);
        }
    }








    public Server(InetAddress host, int portTCP) {
        this.host = host;
        this.portTCP = portTCP;
        ports = new ArrayList<>();
        orders = new Orders<>();
        for (int i = TCP_PORT_MIN; i < TCP_PORT_MAX; i++) {
            ports.add(i);
        }
    }






    public void sendPort() {

        synchronized (ports) {
            while (true) {
                try {

                    System.out.println("Рассылка портов");
                    byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(portTCP).array();
                    for (int i = 0; i < ports.size(); i++) {
                        DatagramPacket packet = new DatagramPacket(data, data.length, broadcast, ports.get(i).intValue());
                        DatagramSocket socket = new DatagramSocket();
                        socket.send(packet);
                        socket.close();

                    }

                    TimeUnit.SECONDS.sleep(TIMEOUT);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }




    public void getData() {

        while (true) {
            System.out.println("Ожидание подключения клиента...");
            try (ServerSocket server = new ServerSocket(portTCP)) {

                Socket client = server.accept();
                System.out.println("Клиент подключен");

                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                int port = in.readInt();
                //ports.remove((Integer) port);
                Order order = (Order) in.readObject();
                order.setPort(port);
                order.setInetAddress(client.getInetAddress());
                order.read();
                orders.add(order);
                in.close();
                client.close();
                TimeUnit.SECONDS.sleep(TIMEOUT);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Заказ получен");
        }
    }







    public void sentStatus(int index) {
        try {
            int status = 1;
            byte[] data = ByteBuffer.allocate(SIZE_OF_INT).putInt(status).array();
            System.out.println("Заказ " + orders.get(index).getCredentialsID() + " обработан");
            System.out.println("IP: " + orders.get(index).getInetAddress().toString() + "   Port: " + orders.get(index).getPort());
            DatagramPacket packet = new DatagramPacket(data, data.length, orders.get(index).getInetAddress(), orders.get(index).getPort());
            DatagramSocket socket = new DatagramSocket();
            socket.send(packet);
            socket.close();
            orders.get(index).setSentStatus(true);
            //ports.add(orders.get(index).getPort());
            TimeUnit.SECONDS.sleep(TIMEOUT);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }





    public void run() {
        Thread threadPort = new Thread(this::sendPort);
        threadPort.start();
        Thread threadData = new Thread(this::getData);
        threadData.start();
        Thread threadProcessing = new Thread(this::processing);
        threadProcessing.start();
    }







    public void processing() {
        while (true) {
            ACheck statusFalse;
            ACheck statusTrue;

            try {
                statusFalse = new ACheckFalse(orders);
                statusFalse.getThread().join();



                    for (int i = 0; i < orders.size(); i++) {
                        if (orders.get(i).getStatus()) {
                            sentStatus(i);
                            System.out.println(" === " + i + " === ");
                        }
                    }
                    /*for (Iterator iter =orders.getOrders().iterator();iter.hasNext();i++) {
                        Order order= (Order) iter.next();
                        if (order.getStatus()) {
                            sentStatus(i);
                            System.out.println(" === " + i + " === ");
                        }
                    }

                SEMAPHORE.acquire();
                statusTrue = new ACheckTrue(orders);
                statusTrue.getThread().join();
                SEMAPHORE.release();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public static void main(String[] args) throws UnknownHostException {
        System.out.println("Сервер запущен...");
        Server server = new Server();
        server.run();
    }
}*/
