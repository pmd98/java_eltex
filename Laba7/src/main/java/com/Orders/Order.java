package com.Orders;

import java.util.Date;
import java.util.UUID;

public class Order  implements ICrudAction {


    private UUID orderId;
    private boolean orderStatus;
    private Date timeOfCreature;
    private Date timeOfWaiting;
    private ShoppingCart shoppingCart;
    private Credentials credentials;


    public Order() {
        this.timeOfCreature = null;
        this.timeOfWaiting = null;
        this.shoppingCart = new ShoppingCart();
        this.credentials = new Credentials();
        this.orderStatus = false;
        this.orderId = UUID.randomUUID();
    }

    public Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;

    }

    @Override
    public void read() {
        System.out.println("\nДанные пользователя:\n");
        credentials.read();
        System.out.println("\nКорзина:\n");
        shoppingCart.read();
        System.out.println("Время создания:" + timeOfCreature);
        System.out.println("Время ожидания:" + timeOfWaiting);
        System.out.print("Статус: ");
        if (this.getStatus())
            System.out.println("\"Выполнен\"");
        else
            System.out.println("\"В ожидании\"");
    }

    public void createOrder(ShoppingCart shoppingCart, Credentials credentials) {
        this.credentials = credentials;
        this.shoppingCart = shoppingCart;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void create() {
        this.credentials.create();
        this.shoppingCart.create();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 2) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void update() {
        this.credentials.update();
        this.shoppingCart.update();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void delete() {
        this.credentials.delete();
        this.shoppingCart.delete();
    }

    public boolean isComplete() {
        Date date = new Date();
        if (timeOfWaiting.getTime() <= date.getTime())
            orderStatus = true;
        else
            orderStatus = false;
        return orderStatus;
    }

    public boolean getStatus() {
        return orderStatus;
    }

    public Date getTimeOfCreature() {
        return this.timeOfCreature;
    }

    public Date getTimeOfWaiting() {
        return this.timeOfWaiting;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public UUID getOrderId() {
        return orderId;
    }


    public void setOrderStatus(String orderStatus) {
        if (orderStatus.equals("Выполнен"))
            this.orderStatus = true;
        else
            this.orderStatus = false;

    }

    public void setOrderStatus(boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setTimeOfCreature(long timeOfCreature) {
        this.timeOfCreature = new Date(timeOfCreature);
    }

    public void setTimeOfWaiting(long timeOfWaiting) {
        this.timeOfWaiting = new Date(timeOfWaiting);
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public void setOrderId(String orderId) {
        UUID uuid =
                UUID.fromString(
                        orderId
                                .replaceFirst(
                                        "(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)",
                                        "$1-$2-$3-$4-$5"
                                )
                );
        this.orderId = uuid;
    }
}