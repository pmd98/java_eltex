package com.company.IOrder;

import com.company.Deserializer.*;
import com.company.Orders.*;
import com.company.Serializer.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.LinkedList;


public class ManagerOrderJSON extends AManagerOrder {


    private static final String jsonFileName = "/home/maksim/IdeaProjects/Laba5/jsonFileName.json";


    public ManagerOrderJSON() {
        super();
    }

    public ManagerOrderJSON(Orders orders) {
        super(orders);
    }

    @Override
    public Order readByID(String id) {
        System.out.println("ManagerOrderJSON - readByID");
        try (BufferedReader reader = new BufferedReader(new FileReader(jsonFileName))) {

            String jsonStr = reader.readLine();
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Coffe.class, new CoffeDeserializer())
                    .registerTypeAdapter(Tea.class, new TeaDeserializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsDeserializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartDeserializer())
                    .registerTypeAdapter(Order.class, new OrderDeserializer())
                    .registerTypeAdapter(Orders.class, new OrdersDeserializer())
                    .create();

            Orders orders = gson.fromJson(jsonStr, Orders.class);
            for (int i = 0; i < orders.size(); ++i) {
                if (orders.get(i).getCredentialsID().toString().equals(id))
                    return orders.get(i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Нет такого ID");
        return null;
    }

    @Override
    public void saveByID(String id) {
        System.out.println("ManagerOrderJSON - saveByID");
        int i = 0;
        for (; i < this.orders.size(); ++i) {
            if (this.orders.get(i).getCredentialsID().toString().equals(id))
                break;
        }
        if (i == this.orders.size()) {
            System.out.println("Нет такого ID");
            return;
        }
        Orders orders = this.readAll();
        orders.add(this.orders.get(i));

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(jsonFileName))) {

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Coffe.class, new CoffeSerializer())
                    .registerTypeAdapter(Tea.class, new TeaSerializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsSerializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartSerializer())
                    .registerTypeAdapter(Order.class, new OrderSerializer())
                    .registerTypeAdapter(Orders.class, new OrdersSerializer())
                    .create();

            StringBuilder string = new StringBuilder(gson.toJson(orders));
            string.append('\n');
            gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .registerTypeAdapter(Coffe.class, new CoffeSerializer())
                    .registerTypeAdapter(Tea.class, new TeaSerializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsSerializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartSerializer())
                    .registerTypeAdapter(Order.class, new OrderSerializer())
                    .registerTypeAdapter(Orders.class, new OrdersSerializer())
                    .create();
            string.append(gson.toJson(orders));
            writer.write(string.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Сохранено");
    }

    @Override
    public Orders readAll() {
        System.out.println("ManagerOrderJSON - readAll");
        try (BufferedReader reader = new BufferedReader(new FileReader(jsonFileName))) {

            String jsonStr = reader.readLine();
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Coffe.class, new CoffeDeserializer())
                    .registerTypeAdapter(Tea.class, new TeaDeserializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsDeserializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartDeserializer())
                    .registerTypeAdapter(Order.class, new OrderDeserializer())
                    .registerTypeAdapter(Orders.class, new OrdersDeserializer())
                    .create();

            Orders orders = gson.fromJson(jsonStr, Orders.class);
            for (int i = 0; i < orders.size(); ++i) {
                this.orders.add(orders.get(i));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public void saveAll() {
        System.out.println("ManagerOrderJSON - saveAll");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(jsonFileName))) {

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Coffe.class, new CoffeSerializer())
                    .registerTypeAdapter(Tea.class, new TeaSerializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsSerializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartSerializer())
                    .registerTypeAdapter(Order.class, new OrderSerializer())
                    .registerTypeAdapter(Orders.class, new OrdersSerializer())
                    .create();

            StringBuilder string = new StringBuilder(gson.toJson(orders));
            string.append('\n');
            gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .registerTypeAdapter(Coffe.class, new CoffeSerializer())
                    .registerTypeAdapter(Tea.class, new TeaSerializer())
                    .registerTypeAdapter(Credentials.class, new CredentialsSerializer())
                    .registerTypeAdapter(ShoppingCart.class, new ShoppingCartSerializer())
                    .registerTypeAdapter(Order.class, new OrderSerializer())
                    .registerTypeAdapter(Orders.class, new OrdersSerializer())
                    .create();
            string.append(gson.toJson(orders));
            writer.write(string.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}