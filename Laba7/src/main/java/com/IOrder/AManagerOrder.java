package com.IOrder;

import com.Orders.Order;
import com.Orders.Orders;

public abstract class AManagerOrder implements IOrder {

    public AManagerOrder() {

        orders = new Orders();
    }

    public AManagerOrder(Orders orders) {

        this.orders = orders;
    }

    @Override
    public Order readByID(String id) {

        System.out.println("AManagerOrder - readByID");
        return null;
    }


    @Override
    public void saveByID(String id) {

        System.out.println("AManagerOrder - saveByID");
    }

    @Override
    public Orders readAll() {

        System.out.println("AManagerOrder - readAll");
        return null;
    }

    @Override
    public void saveAll() {

        System.out.println("AManagerOrder - saveAll");
    }


    protected Orders orders;

}