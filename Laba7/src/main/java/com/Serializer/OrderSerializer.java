package com.Serializer;

import java.lang.reflect.Type;

import com.Orders.Order;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class OrderSerializer implements JsonSerializer<Order> {
    @Override
    public JsonElement serialize(Order src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();

        result.addProperty("Id заказа", src.getOrderId().toString());
        result.addProperty("Время создания", src.getTimeOfCreature().getTime());
        result.addProperty("Время ожидания", src.getTimeOfWaiting().getTime());
        if (src.getStatus())
            result.addProperty("Статус заказа", "Выполнен");
        else
            result.addProperty("Статус заказа", "В ожидании");
        result.add("Данные пользователя", context.serialize(src.getCredentials()));
        result.add("Корзина", context.serialize(src.getShoppingCart()));
        return result;
    }
}