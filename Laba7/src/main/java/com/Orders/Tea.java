package com.Orders;

import java.lang.reflect.Type;
import java.util.Scanner;
import java.util.UUID;

public class Tea extends Product {

    private String kindOfPacking; //Вид упоковки

    public Tea() {
        super();
        kindOfPacking = null;
        this.productID = UUID.randomUUID();
    }

    public Tea(String name, String country_manufacturer, String company_supplier, String kindOfPacking, int cost) {
        this.name = name;
        this.countryManufacturer = country_manufacturer;
        this.companySupplier = company_supplier;
        this.cost = cost;
        this.kindOfPacking = kindOfPacking;
        Tea.counter++;
    }

    @Override
    public void create() {
        /* */
        String name[] = {"Lipton", "Принцесса Нури", "TESS", "GreenField", "Dilmah"};
        String country_manufacturer[] = {"Бразилия", "Вьетнам", "Индонезия", "Колумбия", "Эфиопия"};
        String company_supplier[] = {"Uniliver", "Ahmad Tea", "GreenField", "Dilmah", "TESS"};
        String kind_of_packing[] = {"Картонная", "Бумажная", "Жестянная", "Керамическая", "Деревянная"};

        /* */
        final int range_max = 5;

        int random_number = (int) (Math.random() * 10 % (range_max));

        this.name = name[random_number];
        this.countryManufacturer = country_manufacturer[random_number];
        this.companySupplier = company_supplier[random_number];
        this.kindOfPacking = kind_of_packing[random_number];
        this.cost = 100 + (double) (random_number * random_number * random_number * 100) / (double) (3);
        Tea.counter++;
    }

    @Override
    public void update() {
        Scanner tea = new Scanner(System.in);

        System.out.print("Введите название товара: ");
        this.name = tea.nextLine();

        System.out.print("Введите страну производителя: ");
        this.countryManufacturer = tea.nextLine();

        System.out.print("Введите фирму поставщика: ");
        this.companySupplier = tea.nextLine();

        System.out.print("Введите вид упоковки: ");
        this.kindOfPacking = tea.nextLine();

        System.out.print("Введите стоимость: ");
        this.cost = tea.nextDouble();

    }

    @Override
    public void delete() {

        this.kindOfPacking = null;
        this.productID = null;
        this.companySupplier = null;
        this.countryManufacturer = null;
        this.cost = 0;
        this.name = null;

        Tea.counter--;
    }

    @Override
    public void read() {
        System.out.println("ID товара:\t" + this.productID);

        System.out.println("Название:\t" + this.name);

        System.out.println("Страна производитель:\t" + this.countryManufacturer);

        System.out.println("Фирма поставщик:\t" + this.companySupplier);

        System.out.println("Цена:\t" + this.cost);

        System.out.println("Вид упоковки:\t" + this.kindOfPacking);
    }


    public static String whoAmI() {
        return "Чай";
    }

    @Override
    public String whoseAmI() {
        return "Чай";
    }

    public void setKindOfPacking(String kindOfPacking) {
        this.kindOfPacking = kindOfPacking;
    }

    public String getKindOfPacking() {
        return kindOfPacking;
    }
}
