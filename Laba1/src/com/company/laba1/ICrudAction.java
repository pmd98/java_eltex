package com.company.laba1;

public interface ICrudAction {

    void create(); //  заполнение объекта случайными значениями и инкремент счётчика.

    void update(); //  ввод данных с клавиатуры

    void delete(); //  принудительное зануление данных в объекте и декремент счетчика

    void read(); //    вывод данных на экран.

}
