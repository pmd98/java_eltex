package com.company.Orders;
import java.util.UUID;

public  abstract class Product implements ICrudAction {

    public Product() {
        this.productID = null;
        this.companySupplier = null;
        this.countryManufacturer = null;
        this.cost = 0;
        this.name = null;
    }

    @Override
    public abstract void create();//  заполнение объекта случайными значениями и инкремент счётчика.

    @Override
    public abstract void update(); //  ввод данных с клавиатуры

    @Override
    public abstract void delete(); //  принудительное зануление данных в объекте и декремент счетчика

    @Override
    public abstract void read(); //    вывод данных на экран.


    public static String whoAmI(){return "Product";}

    public abstract String whoseAmI();

    public UUID getProductID() {
        return productID;
    }

    public double getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public String getCompanySupplier() {
        return companySupplier;
    }

    public String getCountryManufacturer() {
        return countryManufacturer;
    }

    public static int getCounter() {
        return counter;
    }

    public void setProductID(String productID) {
        UUID uuid =
                UUID.fromString (
                        productID
                                .replaceFirst (
                                        "(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)",
                                        "$1-$2-$3-$4-$5"
                                )
                );
        this.productID = uuid;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }

    public void setCountryManufacturer(String countryManufacturer) {
        this.countryManufacturer = countryManufacturer;
    }

    public static void setCounter(int counter) {
        Product.counter = counter;
    }



    protected UUID productID;               //  ID товара
    protected double cost;                 //  Цена
    protected String name;                  //  Название
    protected String companySupplier;      //  Фирма поставщик
    protected String countryManufacturer;  //  Страна производитель

    protected static int counter;           //  Счётчик товаров


}