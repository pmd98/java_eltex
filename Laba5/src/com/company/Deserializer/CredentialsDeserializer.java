package com.company.Deserializer;

import com.company.Orders.Credentials;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class CredentialsDeserializer implements JsonDeserializer<Credentials> {
    @Override
    public Credentials deserialize(JsonElement json, Type typeOfSrc, JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Credentials credentials = new Credentials();

        credentials.setUserID(jsonObject.get("ID пользователя").getAsString());
        credentials.setName(jsonObject.get("Имя").getAsString());
        credentials.setSurname(jsonObject.get("Фамилия").getAsString());
        credentials.setSecondName(jsonObject.get("Отчество").getAsString());
        credentials.setEmail(jsonObject.get("email").getAsString());

        return credentials;

    }
}
