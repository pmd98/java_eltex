package com.company.Serializer;

import com.company.Orders.Order;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class OrderSerializer implements JsonSerializer<Order> {
    @Override
    public JsonElement serialize(Order src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();

        result.addProperty("timeOfCreature", src.getTimeOfCreature().getTime());
        result.addProperty("Время ожидания", src.getTimeOfWaiting().getTime());
        if (src.getStatus())
            result.addProperty("Статус заказа", "Выполнен");
        else
            result.addProperty("Статус заказа", "В ожидании");
        result.add("Данные пользователя", context.serialize(src.getCredentials()));
        result.add("Корзина", context.serialize(src.getShoppingCart()));
        return result;
    }
}