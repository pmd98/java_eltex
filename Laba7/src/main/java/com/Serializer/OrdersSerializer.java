package com.Serializer;

import com.Orders.Orders;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

public class OrdersSerializer implements JsonSerializer<Orders> {
    @Override
    public JsonElement serialize(Orders src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject result = new JsonObject();
        int i = 1;
        for (Object orders : src.getOrders()) {
            StringBuilder builder = new StringBuilder("Заказ №");
            builder.append(i);
            result.add(builder.toString(), context.serialize(orders));
            ++i;
        }
        return result;
    }
}
