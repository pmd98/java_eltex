package com.company;

import com.company.ACheck.ACheck;
import com.company.ACheck.ACheckCreator;
import com.company.ACheck.ACheckFalse;
import com.company.ACheck.ACheckTrue;
import com.company.IOrder.ManagerOrderFile;
import com.company.IOrder.ManagerOrderJSON;
import com.company.Orders.Order;
import com.company.Orders.Orders;


import java.util.Scanner;

public class Main {


    private static void menu() {
        System.out.println("\n               МЕНЮ ");
        System.out.println(" 1 -- Сохранить в бинарный файл");
        System.out.println(" 2 -- Сохранить в файл JSON");
        System.out.println(" 3 -- Чтение из бинарного файла");
        System.out.println(" 4 -- Чтение из файла JSON");
        System.out.println(" 5 -- Сохранить в бинарный файл по ID");
        System.out.println(" 6 -- Сохранить в файл JSON по ID");
        System.out.println(" 7 -- Чтение из бинарного файла по ID");
        System.out.println(" 8 -- Чтение из файла JSON по ID");
        System.out.println(" 9 -- Добавление заказов ");
        System.out.println(" 10 - Очистить заказы");
        System.out.println(" 11 - Вывести заказы");
        System.out.println(" 0  - Выход");
    }

    public static void main(String[] args) {
        final int VALUE = 10;
        int var = 1;
        Orders orders = new Orders();
        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON(orders);
        Scanner in = new Scanner(System.in);
        Scanner str = new Scanner(System.in);
        System.out.println("------------------------------------------------------------");
        while (var != 0) {
            menu();
            System.out.println(">>");
            var = in.nextInt();
            switch (var) {
                case 0: {
                    break;
                }
                case 1: {
                    managerOrderFile.saveAll();
                    System.out.println("Сохранено");
                    break;
                }
                case 2: {
                    managerOrderJSON.saveAll();
                    System.out.println("Сохранено");
                    break;
                }
                case 3: {
                    managerOrderFile.readAll();
                    System.out.println("Прочитано");
                    break;
                }
                case 4: {
                    managerOrderJSON.readAll();
                    System.out.println("Прочитано");
                    break;
                }
                case 5: {
                    String id;
                    System.out.println("Введите ID:");
                    id = str.nextLine();
                    managerOrderFile.saveByID(id);
                    break;
                }
                case 6: {
                    String id;
                    System.out.println("Введите ID:");
                    id = str.nextLine();
                    managerOrderJSON.saveByID(id);
                    break;
                }
                case 7: {
                    String id;
                    System.out.println("Введите ID:");
                    id = str.nextLine();
                    Order order = managerOrderFile.readByID(id);
                    if(order!=null){
                    System.out.println("Найденный заказ:");
                    order.read();}
                    break;
                }
                case 8: {
                    String id;
                    System.out.println("Введите ID:");
                    id = str.nextLine();
                    Order order = managerOrderJSON.readByID(id);
                    if(order!=null){
                    System.out.println("Найденный заказ:");
                    order.read();}
                    break;
                }
                case 9: {
                    orders.create();
                    break;
                }
                case 10: {
                    orders.delete();
                    break;
                }
                case 11: {
                    orders.read();
                    break;
                }
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }
            }
        }

        in.close();
    }

}