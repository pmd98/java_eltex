package com.ACheck;

import com.Orders.Order;
import com.Orders.Orders;

public class ACheckCreator< T extends Order> extends ACheck {

    public ACheckCreator() {
        super();
    }

    public ACheckCreator(Orders<T> orders) {
        super(orders);
    }


    @Override
    public void run() {
        super.run();
        //System.out.println("\nACheckCreator\n");
        orders.create();
    }
}