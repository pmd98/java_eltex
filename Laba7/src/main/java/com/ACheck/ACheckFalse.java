package com.ACheck;

import com.Orders.Order;
import com.Orders.Orders;

import java.util.Iterator;

public class ACheckFalse<T extends Order> extends ACheck {

    public ACheckFalse() {
        super();
    }

    public ACheckFalse(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
        synchronized (orders) {
          //  System.out.println("\nACheckFalse\n");
            for (Iterator<Order> iter = orders.iterator(); iter.hasNext(); )
            {
                Order order =iter.next();
                order.isComplete();
            }

        }

    }
}
