package com.Deserializer;

import com.Orders.Tea;
import java.lang.reflect.Type;
import com.google.gson.*;

public class TeaDeserializer implements JsonDeserializer<Tea> {
    @Override
    public Tea deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        Tea tea = new Tea();

        tea.setKindOfPacking(jsonObject.get("Вид упаковки").getAsString());
        tea.setProductID(jsonObject.get("ID товара").getAsString().toString());
        tea.setCost(jsonObject.get("Цена").getAsDouble());
        tea.setCompanySupplier(jsonObject.get("Фирма поставщик").getAsString());
        tea.setCountryManufacturer(jsonObject.get("Страна производитель").getAsString());
        tea.setName(jsonObject.get("Название").getAsString());
        return tea;
    }
}