package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Credentials implements ICrudAction {

    /*
    Поля: ID, Фамилия, Имя, Отчество, e-mail
    */
    Credentials() {
        this.id = UUID.randomUUID();
        ;
        this.email = null;
        this.name = null;
        this.second_name = null;
        this.surname = null;
    }

    Credentials(String name, String surname, String second_name, String email) {
        this.id = UUID.randomUUID();
        this.email = email;
        this.name = name;
        this.second_name = second_name;
        this.surname = surname;

        Credentials.counter++;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void read() {
        System.out.println("ID пользователя:\t" + this.id);

        System.out.println("Имя: \t" + this.name);

        System.out.println("Фамилия: \t" + this.surname);

        System.out.println("Отчество: \t" + this.second_name);

        System.out.println("email: \t" + this.email);
    }

    @Override
    public void create() {
        /* */
        String name[] = {"Владимир", "Алексей", "Дмитрий", "Павел", "Александр"};
        String surname[] = {"Дмитриев", "Владмиров", "Сидоров", "Петров", "Иванов"};
        String second_name[] = {"Иванович", "Петрович", "Владмирович", "Дмитриевич", "Александрович"};
        String email[] = {"@mail.ru", "@yandex.ru", "@google.com", "@rambler.com", "@list.ru"};

        /* */
        final int MAX_RANGE = 5;

        int random_number = (int) (Math.random() * 10 % (MAX_RANGE));

        this.name = name[random_number];
        this.second_name = second_name[random_number];
        this.surname = surname[random_number];
        this.email = name[random_number].concat(email[random_number]);
        this.id = UUID.randomUUID();

        Credentials.counter++;
    }

    @Override
    public void update() {

        System.out.print("Ввод данных пользователя ");
        Scanner user = new Scanner(System.in);

        System.out.print("Введите имя пользователя: ");
        this.name = user.nextLine();

        System.out.print("Введите фамилию: ");
        this.surname = user.nextLine();

        System.out.print("Введите отчество: ");
        this.second_name = user.nextLine();

        System.out.print("Введите вид email: ");
        this.email = user.nextLine();

        this.id = UUID.randomUUID();
    }

    @Override
    public void delete() {

        this.id = null;
        this.name = null;
        this.surname = null;
        this.second_name = null;
        this.email = null;

        Credentials.counter--;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private UUID id;
    private String name;
    private String surname;
    private String second_name;
    private String email;

    private static int counter;
}