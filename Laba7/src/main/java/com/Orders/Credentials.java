package com.Orders;

import java.lang.reflect.Type;
import java.util.Scanner;
import java.util.UUID;

public class Credentials implements ICrudAction {

    private UUID userID;
    private String name;
    private String surname;
    private String secondName;
    private String email;

    private static int counter;
    /*
    Поля: ID, Фамилия, Имя, Отчество, e-mail
    */

    public Credentials() {
        this.userID = UUID.randomUUID();
        this.email = null;
        this.name = null;
        this.secondName = null;
        this.surname = null;
    }

    public Credentials(String name, String surname, String secondName, String email) {
        this.userID = UUID.randomUUID();
        this.email = email;
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;

        Credentials.counter++;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void read() {
        System.out.println("ID пользователя:\t" + this.userID);

        System.out.println("Имя: \t" + this.name);

        System.out.println("Фамилия: \t" + this.surname);

        System.out.println("Отчество: \t" + this.secondName);

        System.out.println("email: \t" + this.email);
    }

    @Override
    public void create() {
        /* */
        String name[] = {"Владимир", "Алексей", "Дмитрий", "Павел", "Александр"};
        String surname[] = {"Дмитриев", "Владмиров", "Сидоров", "Петров", "Иванов"};
        String second_name[] = {"Иванович", "Петрович", "Владмирович", "Дмитриевич", "Александрович"};
        String email[] = {"@mail.ru", "@yandex.ru", "@google.com", "@rambler.com", "@list.ru"};

        /* */
        final int MAX_RANGE = 5;

        int random_number = (int) (Math.random() * 10 % (MAX_RANGE));

        this.name = name[random_number];
        this.secondName = second_name[random_number];
        this.surname = surname[random_number];
        this.email = name[random_number].concat(email[random_number]);
        Credentials.counter++;
    }

    @Override
    public void update() {

        System.out.print("Ввод данных пользователя ");
        Scanner user = new Scanner(System.in);

        System.out.print("Введите имя пользователя: ");
        this.name = user.nextLine();

        System.out.print("Введите фамилию: ");
        this.surname = user.nextLine();

        System.out.print("Введите отчество: ");
        this.secondName = user.nextLine();

        System.out.print("Введите вид email: ");
        this.email = user.nextLine();
    }

    @Override
    public void delete() {

        this.userID = null;
        this.name = null;
        this.surname = null;
        this.secondName = null;
        this.email = null;

        Credentials.counter--;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public UUID getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getEmail() {
        return email;
    }


    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public void setUserID(String userId) {
        UUID uuid =
                UUID.fromString(
                        userId
                                .replaceFirst(
                                        "(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)",
                                        "$1-$2-$3-$4-$5"
                                )
                );
        this.userID = uuid;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}