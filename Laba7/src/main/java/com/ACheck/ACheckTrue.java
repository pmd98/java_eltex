package com.ACheck;

import com.Orders.Orders;
import com.Orders.Order;

import java.util.Iterator;

public class ACheckTrue<T extends Order> extends ACheck {

    public ACheckTrue() {
        super();
    }

    public ACheckTrue(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
       // System.out.println("\nACheckTrue\n");

        synchronized (orders) {

            for ( Iterator<Order> iter = orders.iterator(); iter.hasNext(); ) {

                Order order = iter.next();
                if (order.getStatus()) {
                    iter.remove();
                }
            }
        }

    }
}
