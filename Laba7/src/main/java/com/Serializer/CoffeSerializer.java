package com.Serializer;

import com.Orders.Coffe;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class CoffeSerializer implements JsonSerializer<Coffe> {
    @Override
    public JsonElement serialize(Coffe src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();

        result.addProperty("ID товара", src.getProductID().toString());
        result.addProperty("Цена", src.getCost());
        result.addProperty("Название", src.getName());
        result.addProperty("Фирма поставщик", src.getCompanySupplier());
        result.addProperty("Страна производитель", src.getCountryManufacturer());
        result.addProperty("Вид кофейных зерен", src.getKindOfCoffeeBeans());
        result.addProperty("Продукт", src.whoseAmI());
        return result;
    }
}