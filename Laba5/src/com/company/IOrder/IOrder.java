package com.company.IOrder;

import com.company.Orders.Order;
import com.company.Orders.Orders;

public interface IOrder<T extends Order> {

    public Order readByID(String id);

    public void saveByID(String id);

    public Orders<T> readAll();

    public void saveAll();

}
