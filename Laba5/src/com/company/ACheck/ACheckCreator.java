package com.company.ACheck;

import com.company.Orders.Order;
import com.company.Orders.Orders;

public class ACheckCreator< T extends Order> extends ACheck {

    public ACheckCreator() {
        super();
    }

    public ACheckCreator(Orders<T> orders) {
        super(orders);
    }


    @Override
    public void run() {
        super.run();
        //System.out.println("\nACheckCreator\n");
        orders.create();
    }
}