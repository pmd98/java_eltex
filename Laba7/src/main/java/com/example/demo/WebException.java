package com.example.demo;

public class WebException extends Exception {

    private int number;
    private String message;

    public WebException(int errorNumber) {
        super(String.valueOf(errorNumber));
        number=errorNumber;
        switch (errorNumber)
        {
            case 1: {
                message="Incorrect Input ID!";
                break;
            }
            case 2:{
                message="Can not open file!";
                break;
            }
            case 3:{
                message="Incorrect Operation!";
                break;
            }
            default:{
                message="Unknown message";
                break;
            }
        }
    }

    public WebException(String message) {
        super(message);
        this.message=message;
        switch (message)
        {
            case "Incorrect Input ID!": {
                number=1;
                break;
            }
            case "Can not open file!":{
                number=2;
                break;
            }
            case "Incorrect Operation!":{
                number=3;
                break;
            }
            default:{
                number=-1;
                break;
            }
        }
    }

    public int getNumber() {
        return number;
    }
    public String getMessage() {
        return message;
    }
}
