package com.Serializer;

import java.lang.reflect.Type;

import com.Orders.Credentials;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CredentialsSerializer implements JsonSerializer<Credentials> {
    @Override
    public JsonElement serialize(Credentials src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();

        result.addProperty("ID пользователя", src.getUserID().toString());
        result.addProperty("Имя", src.getName());
        result.addProperty("Фамилия", src.getSurname());
        result.addProperty("Отчество", src.getSecondName());
        result.addProperty("email", src.getEmail());
        return result;
    }
}
