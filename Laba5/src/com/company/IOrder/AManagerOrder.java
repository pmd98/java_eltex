package com.company.IOrder;

import com.company.Orders.Order;
import com.company.Orders.Orders;

public abstract class AManagerOrder implements IOrder {

    public AManagerOrder() {

        orders = new Orders();
    }

    public AManagerOrder(Orders orders) {

        this.orders = orders;
    }

    public void readById(String id) {

        System.out.println("AManagerOrder - readByID");
    }


    public void saveById(String id) {

        System.out.println("AManagerOrder - saveByID");
    }

    @Override
    public Orders readAll() {

        System.out.println("AManagerOrder - readAll");
        return null;
    }

    @Override
    public void saveAll() {

        System.out.println("AManagerOrder - saveAll");
    }


    protected Orders orders;

}