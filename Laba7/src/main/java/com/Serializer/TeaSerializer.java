package com.Serializer;

import java.lang.reflect.Type;

import com.Orders.Tea;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class TeaSerializer implements JsonSerializer<Tea> {
    @Override
    public JsonElement serialize(Tea src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();

        result.addProperty("ID товара", src.getProductID().toString());
        result.addProperty("Цена", src.getCost());
        result.addProperty("Название", src.getName());
        result.addProperty("Фирма поставщик", src.getCompanySupplier());
        result.addProperty("Страна производитель", src.getCountryManufacturer());
        result.addProperty("Вид упаковки", src.getKindOfPacking());
        result.addProperty("Продукт", src.whoseAmI());
        return result;
    }
}