package com.company.Orders;
import java.util.Scanner;
import java.util.UUID;

public class Coffe extends Product {

    public Coffe() {
        super();
        kindOfCoffeeBeans = null;
    }

    Coffe(String name, String country_manufacturer, String company_supplier, String kindOfCoffeeBeans, int cost) {
        this.name = name;
        this.countryManufacturer = country_manufacturer;
        this.companySupplier = company_supplier;
        this.cost = cost;
        this.kindOfCoffeeBeans = kindOfCoffeeBeans;
        Coffe.counter++;
    }


    @Override
    public void create() {

        String name[] = {"Черная карта", "CORTE NOIRE", "Jardin", "Ambassador", "Жокей"};
        String country_manufacturer[] = {"Бразилия", "Вьетнам", "Индонезия", "Колумбия", "Эфиопия"};
        String company_supplier[] = {"lavAzza", "Bravas", "BestCoffee", "Coffe Vend", "Caffe Boast"};
        String kind_of_coffee_beans[] = {"Арабика", "Робуста"};

        final int range_max = 5;

        int random_number = (int) (Math.random() * 10 % (range_max));

        this.name = name[random_number];
        this.countryManufacturer = country_manufacturer[random_number];
        this.companySupplier = company_supplier[random_number];
        this.kindOfCoffeeBeans = kind_of_coffee_beans[random_number % 2];

        this.cost = 150 + (double) (random_number * random_number * random_number * 100) / (double) (3);
        this.productID = UUID.randomUUID();


        Coffe.counter++;
    }

    @Override
    public void update() {
        Scanner coffe = new Scanner(System.in);

        System.out.print("Введите название товара: ");
        this.name = coffe.nextLine();

        System.out.print("Введите страну производителя: ");
        this.countryManufacturer = coffe.nextLine();

        System.out.print("Введите фирму поставщика: ");
        this.companySupplier = coffe.nextLine();

        System.out.print("Введите тип кофейных зерен: ");
        this.kindOfCoffeeBeans = coffe.nextLine();

        System.out.print("Введите стоимость: ");
        this.cost = coffe.nextDouble();

        // coffe.close();
    }

    @Override
    public void delete() {

        this.kindOfCoffeeBeans = null;
        this.productID = null;
        this.companySupplier = null;
        this.countryManufacturer = null;
        this.cost = 0;
        this.name = null;

        Coffe.counter--;

    }

    @Override
    public void read() {

        System.out.println("ID товара:\t" + this.productID);

        System.out.println("Название:\t" + this.name);

        System.out.println("Страна производитель:\t" + this.countryManufacturer);

        System.out.println("Фирма поставщик:\t" + this.companySupplier);

        System.out.println("Цена:\t" + this.cost);

        System.out.println("Вид кофейных зерен:\t" + this.kindOfCoffeeBeans);

    }


    public static String whoAmI() {
        return "Кофе";
    }

    @Override
    public String whoseAmI() {
        return "Кофе";
    }

    public void setKindOfCoffeeBeans(String kindOfCoffeeBeans) {
        this.kindOfCoffeeBeans = kindOfCoffeeBeans;
    }

    public String getKindOfCoffeeBeans() {
        return kindOfCoffeeBeans;
    }

    //private Packing type
    private String kindOfCoffeeBeans; //Вид кофейных зерен

}
