package com.company.Orders;

import java.util.Date;

public class Order extends Orders implements ICrudAction {

    @Override
    public void read() {
        System.out.println("\nДанные пользователя:\n");
        credentials.read();
       // System.out.println("\nКорзина:\n");
       // shoppingCart.read();
        System.out.println("Время создания:" + timeOfCreature);
        System.out.println("Время ожидания:" + timeOfWaiting);
        System.out.print("Статус: ");
        if (this.getStatus())
            System.out.println("\"Выполнен\"");
        else
            System.out.println("\"В ожидании\"");
    }

    public Order() {
        this.timeOfCreature = null;
        this.timeOfWaiting = null;
        this.shoppingCart = new ShoppingCart();
        this.credentials = new Credentials();
        this.orderStatus = false;
    }

    public Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;

    }

    public void create_Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.credentials = credentials;
        this.shoppingCart = shoppingCart;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void create() {
        this.credentials.create();
        this.shoppingCart.create();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 2) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void update() {
        this.credentials.update();
        this.shoppingCart.update();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
    }

    @Override
    public void delete() {
        this.credentials.delete();
        this.shoppingCart.delete();
    }

    public boolean is_complete() {
        Date date = new Date();
        if (timeOfWaiting.getTime() <= date.getTime())
            orderStatus = true;
        else
            orderStatus = false;
        return orderStatus;
    }

    public boolean getStatus() {
        return orderStatus;
    }

    public Date getTimeOfCreature() {
        return this.timeOfCreature;
    }

    public Date getTimeOfWaiting() {
        return this.timeOfWaiting;
    }

    private boolean orderStatus;
    private Date timeOfCreature;
    private Date timeOfWaiting;
    private ShoppingCart shoppingCart;
    private Credentials credentials;
}