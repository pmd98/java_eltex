package com.company.ACheck;

import com.company.Orders.Order;
import com.company.Orders.Orders;

public class ACheckFalse<T extends Order> extends ACheck {

    public ACheckFalse() {
        super();
    }

    public ACheckFalse(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
      //  synchronized (orders) {
            System.out.println("\nACheckFalse\n");
            for (int i = 0; i < orders.size(); ++i) {
                orders.get(i).is_complete();
            }
      //  }

    }
}
