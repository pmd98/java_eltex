package com.company;
import com.company.Orders.Orders;


import java.util.Scanner;

public class Main {


    private static void menu() {
        System.out.println("\n               МЕНЮ ");
        System.out.println(" 1 -- Сохранить в бинарный файл");
        System.out.println(" 2 -- Сохранить в файл JSON");
        System.out.println(" 3 -- Чтение из бинарного файла");
        System.out.println(" 4 -- Чтение из файла JSON");
        System.out.println(" 5 -- Сохранить в бинарный файл по ID");
        System.out.println(" 6 -- Сохранить в файл JSON по ID");
        System.out.println(" 7 -- Чтение из бинарного файла по ID");
        System.out.println(" 8 -- Чтение из файла JSON по ID");
        System.out.println(" 9 -- Добавление заказов ");
        System.out.println(" 10 - Очистить заказы");
        System.out.println(" 11 - Вывести заказы");
        System.out.println(" 0  - Выход");
    }

    public static void main(String[] args) {
        final int VALUE = 10;
        int var = 1;
        Orders orders = new Orders();
        Scanner in = new Scanner(System.in);
        Scanner str = new Scanner(System.in);
        System.out.println("------------------------------------------------------------");
        while (var != 0) {
            menu();
            System.out.println(">>");
            var = in.nextInt();
            switch (var) {
                case 0: {
                    break;
                }

                case 9: {
                    orders.create();
                    break;
                }
                case 10: {
                    orders.delete();
                    break;
                }
                case 11: {
                    orders.read();
                    break;
                }
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }
            }
        }

        in.close();
    }

}