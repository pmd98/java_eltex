package com.company.ACheck;

import com.company.Network.Server;
import com.company.Orders.Order;
import com.company.Orders.Orders;

import java.util.Iterator;

public class ACheckFalse<T extends Order> extends ACheck {

    public ACheckFalse() {
        super();
    }

    public ACheckFalse(Orders<T> orders) {
        super(orders);
    }

    @Override
    public void run() {
        super.run();
        synchronized (orders) {
            for (Iterator<Order> iter = orders.iterator(); iter.hasNext(); )
            {
                Order order =iter.next();
                order.isComplete();
            }

        }

    }
}
