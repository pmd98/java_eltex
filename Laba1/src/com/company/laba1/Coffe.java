package com.company.laba1;

import java.util.Scanner;
import java.util.UUID;

public class Coffe extends Product {

    Coffe()
    {
        super();
        kind_of_coffee_beans =null;
    }

    Coffe(String name,String country_manufacturer,String company_supplier,String kind_of_coffee_beans,int cost)
    {
        this.name                     =     name;
        this.country_manufacturer     =     country_manufacturer;
        this.company_supplier         =     company_supplier;
        this.cost                     =     cost;
        this.kind_of_coffee_beans     =     kind_of_coffee_beans;
        Coffe.counter++;
    }

    @Override
    public void create()
    {
        /* */
        String name[]                   =   {"Черная карта","CORTE NOIRE","Jardin","Ambassador","Жокей"};
        String country_manufacturer[]   =   {"Бразилия","Вьетнам","Индонезия","Колумбия","Эфиопия"};
        String company_supplier[]       =   {"lavAzza","Bravas","BestCoffee","Coffe Vend","Caffe Boast"};
        String kind_of_coffee_beans[]   =   {"Арабика","Робуста"};

        /* */
        final int range_max=5;

        int random_number= (int) (Math.random()*10 % (range_max));

        this.name                     =     name[random_number];
        this.country_manufacturer     =     country_manufacturer[random_number];
        this.company_supplier         =     company_supplier[random_number];
        this.kind_of_coffee_beans     =     kind_of_coffee_beans[random_number%2];

        this.cost                     =   150 +  (double)(random_number*random_number*random_number*100)/(double)(3);
        this.productID                =     UUID.randomUUID();


        Coffe.counter++;
    }

    @Override
    public void update()
    {
        Scanner coffe = new Scanner(System.in);

        System.out.print("Введите название товара: ");
        this.name = coffe.nextLine();

        System.out.print("Введите страну производителя: ");
        this.country_manufacturer = coffe.nextLine();

        System.out.print("Введите фирму поставщика: ");
        this.company_supplier = coffe.nextLine();

        System.out.print("Введите тип кофейных зерен: ");
        this.kind_of_coffee_beans = coffe.nextLine();

        System.out.print("Введите стоимость: ");
        this.cost = coffe.nextDouble();

       // coffe.close();
    }

    @Override
    public void delete() {

        this.kind_of_coffee_beans   = null;
        this.productID              = null;
        this.company_supplier       = null;
        this.country_manufacturer   = null;
        this.cost = 0;
        this.name = null;

        Coffe.counter--;

    }

    @Override
    public void read()
    {
        System.out.println("ID товара:\t"+this.productID);

        System.out.println("Название:\t"+this.name);

        System.out.println("Страна производитель:\t"+this.country_manufacturer);

        System.out.println("Фирма поставщик:\t"+this.company_supplier);

        System.out.println("Цена:\t"+this.cost);

        System.out.println("Вид кофейных зерен:\t"+this.kind_of_coffee_beans);

    }

    //private Packing type
    private String kind_of_coffee_beans; //Вид кофейных зерен

}
