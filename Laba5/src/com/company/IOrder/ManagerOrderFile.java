package com.company.IOrder;

import com.company.Orders.Order;
import com.company.Orders.Orders;

import java.io.*;

public class ManagerOrderFile extends AManagerOrder {


    private static final String binFileName = "/home/maksim/IdeaProjects/Laba5/binFileName.dat";

    public ManagerOrderFile() {
        super();
    }

    public ManagerOrderFile(Orders orders) {
        super(orders);
    }

    @Override
    public Order readByID(String id) {
        System.out.println("ManagerOrderFile - readByID");

        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(binFileName))) {
            Order order;
            boolean mark = false;
            try {
                while (true) {
                    order = (Order) inputStream.readObject();
                    if (order.getCredentialsID().equals(id)) {
                        mark = true;
                        orders.add(order);
                    }
                }
            } catch (EOFException e) {

            }
            if (!mark) {
                System.out.println("Нет такого ID");
                return null;
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveByID(String id) {
        System.out.println("ManagerOrderFile - saveByID");

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(binFileName))) {
            boolean mark = false;
            int count = -1;
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getCredentialsID().equals(id)) {
                    mark = true;
                    count = i;
                }
            }
            if (mark) {
                outputStream.writeObject(orders.get(count));
            } else {
                System.out.println("Нет такого ID");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Сохранено");
    }

    @Override
    public Orders readAll() {

        System.out.println("ManagerOrderFile - readAll");

        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(binFileName))) {
            Order order;
            try {
                while (true) {
                    order = (Order) inputStream.readObject();
                    orders.add(order);
                }
            } catch (EOFException e) {
                e.printStackTrace();
                return orders;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveAll() {

        System.out.println("ManagerOrderFile - saveAll");

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(binFileName))) {
            for (int i = 0; i < orders.size(); i++) {
                outputStream.writeObject(orders.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
