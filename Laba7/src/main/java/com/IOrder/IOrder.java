package com.IOrder;

import com.Orders.Order;
import com.Orders.Orders;

public interface IOrder<T extends Order> {

    public Order readByID(String id);

    public void saveByID(String id);

    public Orders<T> readAll();

    public void saveAll();

}
