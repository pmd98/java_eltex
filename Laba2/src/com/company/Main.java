package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static void choose_product() {
        System.out.println("\n==============  Выбор Объекта  =============");
        System.out.println(" 1 - Чай");
        System.out.println(" 2 - Кофе");
        System.out.println(" 3 - Вывод счетчика объектов");
        System.out.println(" 0 - Выход");
    }

    private static void menu() {
        System.out.println("\n==============  МЕНЮ  ================");
        System.out.println(" 1 - Вывод");
        System.out.println(" 2 - Добавление");
        System.out.println(" 3 - Удаление");
        System.out.println(" 4 - Обновление");
        System.out.println(" 0 - Выход");
    }
    private static void menu1() {
        System.out.println("\n               МЕНЮ ");
        System.out.println(" 1 - Вывод");
        System.out.println(" 2 - Добавление");
        System.out.println(" 3 - Удаление");
        System.out.println(" 4 - Обновление");
        System.out.println(" 5 - Вывод счетчика продуктов");
        System.out.println(" 0 - Выход");
    }


    private static int vybor_do_a(int a) {
        Scanner vybor_do_a = new Scanner(System.in);

        int b;
        do {
            b = vybor_do_a.nextInt();
            if ((b >= 0) && (b <= a)) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);

        // vybor_do_a.close();
        return b;
    }

    private static int vybor() {
        Scanner vybor = new Scanner(System.in);

        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);
        return b;

    }


    public static void main(String[] args) {

        final String _H = "Н";
        final String _h = "н";
        final String _D = "Д";
        final String _d = "д";

        final String _N = "N";
        final String _n = "n";
        final String _Y = "Y";
        final String _y = "y";

        int var = 1;
        int chose = 1;
        String question;

        Scanner in = new Scanner(System.in);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


      //  int random_number = (int)(Math.random()*10+5);

        Orders orders = new Orders();

        Scanner str = new Scanner(System.in);

        while (var != 0) {
            menu1();

            System.out.println(">>");


            var = in.nextInt();

            switch (var) {

                case 0: {
                    break;
                }

                /////////////////////////////ЧТЕНИЕ/////////////////////////////////////////////////////////////////////
                case 1: {
                    orders.read();
                    break;
                }

                /////////////////////////////ДОБАВЛЕНИЕ/////////////////////////////////////////////////////////////////
                case 2: {
                    orders.create();
                    break;
                }
                /////////////////////////////УДАЛЕНИЕ///////////////////////////////////////////////////////////////////
                case 3: {
                    orders.delete();
                    break;
                }

                /////////////////////////////ОБНОВЛЕНИЕ/////////////////////////////////////////////////////////////////
                case 4: {
                    orders.check_orders();
                    break;
                }

                /////////////////////////////ВЫВОД СЧЕТЧИКА/////////////////////////////////////////////////////////////
                case 5: {

                    System.out.println(" \n Текущее число объектов : " + Product.counter);
                    break;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }
            }
        }





       // ArrayList<Credentials> credentials = new ArrayList<>();

       // ArrayList<Product> products = new ArrayList<Product>();

       // ArrayList<Tea> teas = new ArrayList<Tea>(N);

        //ArrayList<Coffe> coffes = new ArrayList<Coffe>(M);

        /*while (var != 0) {
            menu();

            System.out.println(">>");


            var = in.nextInt();

            switch (var) {

                case 0: {
                    break;
                }

                /////////////////////////////ЧТЕНИЕ/////////////////////////////////////////////////////////////////////
                case 1: {
                    for (int i = 0; i < products.size(); i++) {

                        System.out.println("\n Объект номер " + (i + 1) + " :");
                        products.get(i).read();

                    }
                    break;
                }

                /////////////////////////////ДОБАВЛЕНИЕ//////////////////////////////////////////////////////////////////
                case 2: {

                    System.out.println(" Введите количество объектов для добавления (0 для выхода): ");

                    System.out.println("Введите количество объектов [чай]: ");
                    int N = vybor();

                    System.out.println("Введите количество объектов [кофе]: ");
                    int M = vybor();


                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    do {


                        System.out.println(" Заполнить " + N + " объектов [чай] случайными значениями? [Д/Н]");

                        question = null;
                        question = str.nextLine();


                        if (question.equals(_d) || question.equals(_D) || question.equals(_Y) || question.equals(_y)) {


                            for (int i = 0; i < N; i++) {
                                Tea tea = new Tea();
                                tea.create();
                                products.add(tea);
                            }
                            break;
                        } else if (question.equals(_N) || question.equals(_n) || question.equals(_H) || question.equals(_h)) {


                            for (int i = 0; i < N; i++) {
                                if (i == 0)
                                    System.out.println("\t\tВВОД ДАННЫХ [ЧАЙ]:\n");
                                System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                                Tea tea = new Tea();
                                tea.update();
                                products.add(tea);
                            }
                            break;
                        } else {
                            System.out.println(" Некорректный ВВод!");
                        }
                    }
                    while (true);

                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    do {

                        System.out.println(" Заполнить " + M + " объектов [кофе] случайными значениями? [Д/Н]");

                        question = null;
                        question = str.nextLine();


                        if (question.equals(_d) || question.equals(_D) || question.equals(_Y) || question.equals(_y)) {

                            for (int i = 0; i < M; i++) {

                                Coffe coffe = new Coffe();
                                coffe.create();
                                products.add(coffe);
                            }
                            break;
                        } else if (question.equals(_N) || question.equals(_n) || question.equals(_H) || question.equals(_h)) {


                            for (int i = 0; i < M; i++) {
                                if (i == 0)
                                    System.out.println("\t\tВВОД ДАННЫХ [КОФЕ]:\n");
                                System.out.println(" Ввод данных в " + (i + 1) + " объект: ");

                                Coffe coffe = new Coffe();
                                coffe.update();
                                products.add(coffe);
                            }
                            break;

                        } else {
                            System.out.println(" Некорректный ВВод!");
                        }
                    }
                    while (true);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////


                    System.out.println(" Добавлено " + (N+M) + " объектов");
                    break;
                }
                /////////////////////////////////УДАЛЕНИЕ///////////////////////////////////////////////////////////////
                case 3: {

                    System.out.println(" Введите количетсво элементов для удаления (0 для выхода): ");

                    int a = vybor_do_a(products.size());

                    if (a == 0)
                        break;

                    int[] A = new int[a];

                    System.out.println(" Введите номера элементов для удалления: ");

                    for (int i = 0; i < a; i++) {
                        A[i] = vybor_do_a(products.size()) - 1;
                    }

                    for (int i = 0; i < a; i++) {

                        products.get(A[i]).delete();
                        products.remove(A[i]);
                    }

                    System.out.println(" Удалено " + a + " объектов");

                    break;
                }

                ///////////////////////////////////////////////////ОБНОВЛЕНИЕ///////////////////////////////////////////
                case 4: {

                    System.out.println(" Введите номер объектов для обновления : ");
                    int a = vybor_do_a(products.size()) - 1;
                    if (a == -1)
                        break;
                    products.get(a).update();
                    System.out.println(" Изменен " + (a + 1) + " объект");
                    break;
                }

                //////////////////////////////////////////////////ВЫВОД СЧЕТЧИКА////////////////////////////////////////
                case 5: {

                    System.out.println(" \n Текущее число объектов : " + Product.counter);
                    break;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                default: {
                    System.out.println(" Введено неверное число\nПовторите ввод: ");
                    break;
                }
            }
        }
*/
        in.close();
    }


}
