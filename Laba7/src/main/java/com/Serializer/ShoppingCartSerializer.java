package com.Serializer;

import com.Orders.ShoppingCart;
import com.google.gson.*;

import java.lang.reflect.Type;

public class ShoppingCartSerializer implements JsonSerializer<ShoppingCart> {
    @Override
    public JsonElement serialize(ShoppingCart src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject result = new JsonObject();
        JsonArray shoppingCart = new JsonArray();

        result.addProperty("Id корзины", src.getCartId().toString());
        result.add("Содержимое корзины", shoppingCart);
        for (Object cart : src.getList()) {
            shoppingCart.add(context.serialize(cart));
        }

        return result;
    }
}