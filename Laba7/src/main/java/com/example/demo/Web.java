package com.example.demo;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.Orders.*;
import com.IOrder.ManagerOrderJSON;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/")
 public class Web {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private static final Logger loggerToConsole = Logger.getLogger("ConsoleLog");
    private static final Logger loggerToFile = Logger.getLogger("FileLog");

    public Web() {
        Orders<Order> orders = new Orders<>();
        orders.create();
        ManagerOrderJSON json = new ManagerOrderJSON(orders);
        json.saveAll();
    }


    @RequestMapping
    public Object run(@RequestParam String command, @RequestParam(required = false) String order_id, @RequestParam(required = false) String cart_id) {
        switch (command) {
            case "readall": {
                return readAll();
            }
            case "readById": {
                return readById(order_id);
            }
            case "addToCart": {
                return addToCart(cart_id);
            }
            case "delById": {
                return delById(order_id);
            }
            default: {
                try {
                    throw new WebException(3);
                    // throw new WebException("Incorrect Operation!");
                } catch (WebException e) {
                    loggerToConsole.error("Command: \"" + ANSI_CYAN + command + ANSI_RESET + "\" Status: [" + ANSI_RED + "FAIL" + ANSI_RESET + "]. Error massage: \"" + ANSI_RED + e.getMessage() + ANSI_RESET + "\".");
                    loggerToFile.error("Command: " + command + " Status: [FAIL]. Error massage: " + e.getMessage() + ".");
                    return e.getNumber();
                }
            }
        }
    }


    public Orders<Order> readAll() {
        Orders<Order> orders = new Orders<>();
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            if (!json.isOpen()) {
                throw new WebException(2);
                //throw new WebException("Can not open file!");
            }
            json.readAll();
            loggerToConsole.info("Command: \"" + ANSI_CYAN + "readAll" + ANSI_RESET + "\" Status: [" + ANSI_GREEN + "OK" + ANSI_RESET + "].");
            loggerToFile.info("Command: readAll Status: [OK].");

        } catch (WebException e) {
            loggerToConsole.error("Command: \"" + ANSI_CYAN + "readAll" + ANSI_RESET + "\" Status: [" + ANSI_RED + "FAIL" + ANSI_RESET + "]. Error massage: \"" + ANSI_RED + e.getMessage() + ANSI_RESET + "\".");
            loggerToFile.error("Command: readAll Status: [FAIL]. Error massage: " + e.getMessage() + ".");
            return null;
        }
        return orders;
    }


    public Order readById(@RequestParam String order_id) {
        Orders<Order> orders = new Orders<>();
        Order order = null;
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            if (!json.isOpen()) {
                throw new WebException(2);
                //throw new WebException("Can not open file!");
            }
            if ((order = json.readByID(order_id)) == null)
                throw new WebException(1);
            loggerToConsole.info("Command: \"" + ANSI_CYAN + "readById&order_id=" + order_id + ANSI_RESET + "\" Status: [" + ANSI_GREEN + "OK" + ANSI_RESET + "].");
            loggerToFile.info("Command: \"readById&order_id=" + order_id + "\" Status: [OK].");
        } catch (WebException e) {
            loggerToConsole.error("Command: \"" + ANSI_CYAN + "readById&order_id=" + order_id + ANSI_RESET + "\" Status: [" + ANSI_RED + "FAIL" + ANSI_RESET + "]. Error massage: \"" + ANSI_RED + e.getMessage() + ANSI_RESET + "\".");
            loggerToFile.error("Command: readById&order_id=" + order_id + " Status: [FAIL]. Error massage: " + e.getMessage() + ".");
            return null;
        }
        return order;
    }


    public String addToCart(@RequestParam String cart_id) {
        Orders<Order> orders = new Orders<>();
        Product product = null;
        try {
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            if (!json.isOpen()) {
                throw new WebException(2);
                //throw new WebException("Can not open file!");
            }
            json.readAll();
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getShoppingCart().getCartId().toString().equals(cart_id)) {
                    int random_number = (int) (Math.random() * 10 % 2);
                    switch (random_number) {
                        case 0: {
                            product = new Coffe();
                            product.create();
                            break;
                        }
                        case 1: {
                            product = new Tea();
                            product.create();
                            break;
                        }
                    }
                    orders.get(i).getShoppingCart().add(product);
                    break;
                }
            }
            if (product == null) {
                throw new WebException("Incorrect Input ID!");
            }
            json.saveAll();

            loggerToConsole.info("Command: \"" + ANSI_CYAN + "addToCart&cart_id=" + cart_id + ANSI_RESET + "\" Status: [" + ANSI_GREEN + "OK" + ANSI_RESET + "].");
            loggerToFile.info("Command: addToCart&cart_id=" + cart_id + " Status: [OK].");
        } catch (WebException e) {
            loggerToConsole.error("Command: \"" + ANSI_CYAN + "addToCart&cart_id=" + cart_id + ANSI_RESET + "\" Status: [" + ANSI_RED + "FAIL" + ANSI_RESET + "]. Error massage: \"" + ANSI_RED + e.getMessage() + ANSI_RESET + "\".");
            loggerToFile.error("Command: addToCart&cart_id=" + cart_id + " Status: [FAIL]. Error massage: " + e.getMessage() + ".");
            return null;
        }
        return product.getProductID().toString();
    }


    public int delById(@RequestParam String order_id) {
        try {
            Orders<Order> orders = new Orders<>();
            ManagerOrderJSON json = new ManagerOrderJSON(orders);
            if (!json.isOpen()) {
                throw new WebException(2);
                //throw new WebException("Can not open file!");
            }
            json.readAll();
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getOrderId().toString().equals(order_id)) {
                    orders.remove(i);
                    json.saveAll();
                    loggerToConsole.info("Command: \"" + ANSI_CYAN + "delById&order_id=" + order_id + ANSI_RESET + "\" Status: [" + ANSI_GREEN + "OK" + ANSI_RESET + "].");
                    loggerToFile.info("Command: delById&order_id=" + order_id + " Status: [OK].");
                    return 0;
                }
            }
            throw new WebException(1);
            //throw new WebException("Incorrect Input ID!");

        } catch (WebException e) {
            loggerToConsole.error("Command: \"" + ANSI_CYAN + "delById&order_id=" + order_id + ANSI_RESET + "\" Status: [" + ANSI_RED + "FAIL" + ANSI_RESET + "]. Error massage: \"" + ANSI_RED + e.getMessage() + ANSI_RESET + "\".");
            loggerToFile.error("Command: delById&order_id=" + order_id + " Status: [FAIL]. Error massage: " + e.getMessage() + ".");
            return e.getNumber();
        }
    }
}