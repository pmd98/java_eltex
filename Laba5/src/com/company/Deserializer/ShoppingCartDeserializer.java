package com.company.Deserializer;

import com.company.Orders.*;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Map;

public class ShoppingCartDeserializer implements JsonDeserializer<ShoppingCart> {
    @Override
    public ShoppingCart deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException {

        ShoppingCart shoppingCart = new ShoppingCart();
        JsonObject jsonObject = json.getAsJsonObject();
        JsonArray cart = jsonObject.getAsJsonArray("Содержимое корзины");
        for (JsonElement element : cart) {

            JsonObject object;
            object = element.getAsJsonObject();
            if (object.get("Продукт").getAsString().equals(Coffe.whoAmI())) {
                shoppingCart.add(context.deserialize(element, Coffe.class));
            } else if (object.get("Продукт").getAsString().equals(Tea.whoAmI())) {
                shoppingCart.add(context.deserialize(element, Tea.class));
            } else {
                shoppingCart.add(context.deserialize(element, Product.class));
            }
        }
        return shoppingCart;
    }
}