package com.company;

import java.util.*;

public class Orders implements ICrudAction{

    public  Orders()
    {
        this.orders = new LinkedList<Order>();
        this.order_hash  = new HashMap<Date, Order>();
    }

    @Override
    public void read()
    {
        final int N = orders.size();
        for (int i = 0 ; i < N ;i++)
        {
            System.out.println("\nЗаказ №"+(i+1)+":\n");
            this.orders.get(i).read();
        }
    }

    @Override
    public void create() {
        int random_number = (int) (Math.random() * 10 % 3 + 3);

        for (int i = 0; i < random_number; ++i) {
            Order order = new Order();
            order.create();
            orders.add(order);

        }

    }

    @Override
    public void update() {
        Scanner vybor = new Scanner(System.in);

        System.out.println("Ввод количество заказов:");
        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);

        for (int i = 0; i < b; ++i) {
            Order order = new Order();
            System.out.println("Ввод данных в заказ №" + (i + 1) + ": ");
            order.update();
            orders.add(order);
            order_hash.put(order.getTime_of_creature(), order);
        }
    }

    @Override
    public void delete()
    {
        orders.clear();
        order_hash.clear();
    }

    public void issue_purchase(Credentials credential,ShoppingCart shoppingCart)
    {
        Order order = new Order(shoppingCart,credential);
        orders.push(order);
        order_hash.put(order.getTime_of_creature(),order);
    }

    public void check_orders() {
        for (int i = orders.size() - 1; i >= 0; --i) {
            if (orders.get(i).is_complete()) {

                order_hash.remove(orders.get(i).getTime_of_creature());
                orders.get(i).delete();
                orders.remove(i);

            }
        }
    }

    public boolean add(Order order)
    {
       boolean ch = order_hash.containsKey(order.getTime_of_creature());
       if(ch)
           return false;
       order_hash.put(order.getTime_of_creature(),order);
       orders.push(order);
       return true;
    }

    public long size()
    {
    return orders.size();
    }

    private LinkedList<Order> orders;
    private HashMap<Date,Order> order_hash;
}
