package com.company.Orders;

import java.net.InetAddress;
import java.util.Date;
import java.util.UUID;

public class Order  implements ICrudAction {

    @Override
    public void read() {
        System.out.println("\nДанные пользователя:\n");
        credentials.read();
        // System.out.println("\nКорзина:\n");
        //  shoppingCart.read();
        System.out.println("Время создания:" + timeOfCreature);
        System.out.println("Время ожидания:" + timeOfWaiting);
        System.out.print("Статус: ");
        if (this.getStatus())
            System.out.println("\"Выполнен\"");
        else
            System.out.println("\"В ожидании\"");
    }

    public Order() {
        this.timeOfCreature = null;
        this.timeOfWaiting = null;
        this.shoppingCart = new ShoppingCart();
        this.credentials = new Credentials();
        this.orderStatus = false;
        this.sentStatus = false;
        this.inetAddress = null;
        this.port = 0;
    }

    public Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
        this.sentStatus = false;
        this.inetAddress = null;
        this.port = 0;

    }

    public void createOrder(ShoppingCart shoppingCart, Credentials credentials) {
        this.credentials = credentials;
        this.shoppingCart = shoppingCart;
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
        this.sentStatus = false;
        this.inetAddress = null;
        this.port = 0;
    }

    @Override
    public void create() {
        this.credentials.create();
        this.shoppingCart.create();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 3) * 1000));
        this.orderStatus = false;
        this.sentStatus = false;
        this.inetAddress = null;
        this.port = 0;
    }

    @Override
    public void update() {
        this.credentials.update();
        this.shoppingCart.update();
        this.timeOfCreature = new Date();
        this.timeOfWaiting = new Date(timeOfCreature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.orderStatus = false;
        this.sentStatus = false;
        this.inetAddress = null;
        this.port = 0;
    }

    @Override
    public void delete() {
        this.credentials.delete();
        this.shoppingCart.delete();
    }

    public boolean isComplete() {
        Date date = new Date();
        if (timeOfWaiting.getTime() <= date.getTime())
            orderStatus = true;
        else
            orderStatus = false;
        return orderStatus;
    }

    public UUID getCredentialsID() {
        return credentials.getUserID();
    }

    public boolean getStatus() {
        return orderStatus;
    }

    public Date getTimeOfCreature() {
        return this.timeOfCreature;
    }

    public Date getTimeOfWaiting() {
        return this.timeOfWaiting;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public int getPort() {
        return port;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

/////////////////////////////////////////////////////////////////////////////////////////

    public void setOrderStatus(boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        if (orderStatus.equals("true"))
            this.orderStatus = true;
        else
            this.orderStatus = false;

    }

    public void setTimeOfCreature(long timeOfCreature) {
        this.timeOfCreature = new Date(timeOfCreature);
    }

    public void setTimeOfWaiting(long timeOfWaiting) {
        this.timeOfWaiting = new Date(timeOfWaiting);
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }


    public void setInetAddress(InetAddress inetAddress) {
        this.inetAddress = inetAddress;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean getSentStatus() {
        return sentStatus;
    }

    public void setSentStatus(boolean sentStatus) {
        this.sentStatus = sentStatus;
    }

    private boolean orderStatus;
    private boolean sentStatus;
    private Date timeOfCreature;
    private Date timeOfWaiting;
    private ShoppingCart shoppingCart;
    private Credentials credentials;
    private InetAddress inetAddress;
    private int port;
}