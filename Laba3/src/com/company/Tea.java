package com.company;

import com.company.Coffe;
import com.company.Product;

import java.util.Scanner;
import java.util.UUID;

public class Tea extends Product {

    Tea() {
        super();
        kind_of_packing = null;

    }

    Tea(String name, String country_manufacturer, String company_supplier, String kind_of_packing, int cost) {
        this.name = name;
        this.country_manufacturer = country_manufacturer;
        this.company_supplier = company_supplier;
        this.cost = cost;
        this.kind_of_packing = kind_of_packing;
        Coffe.counter++;
    }

    @Override
    public void create() {
        /* */
        String name[] = {"Lipton", "Принцесса Нури", "TESS", "GreenField", "Dilmah"};
        String country_manufacturer[] = {"Бразилия", "Вьетнам", "Индонезия", "Колумбия", "Эфиопия"};
        String company_supplier[] = {"Uniliver", "Ahmad Tea", "GreenField", "Dilmah", "TESS"};
        String kind_of_packing[] = {"Картонная", "Бумажная", "Жестянная", "Керамическая", "Деревянная"};

        /* */
        final int range_max = 5;

        int random_number = (int) (Math.random() * 10 % (range_max));

        this.name = name[random_number];
        this.country_manufacturer = country_manufacturer[random_number];
        this.company_supplier = company_supplier[random_number];
        this.kind_of_packing = kind_of_packing[random_number];

        this.cost = 100 + (double) (random_number * random_number * random_number * 100) / (double) (3);
        this.productID = UUID.randomUUID();


        Coffe.counter++;
    }

    @Override
    public void update() {
        Scanner tea = new Scanner(System.in);

        System.out.print("Введите название товара: ");
        this.name = tea.nextLine();

        System.out.print("Введите страну производителя: ");
        this.country_manufacturer = tea.nextLine();

        System.out.print("Введите фирму поставщика: ");
        this.company_supplier = tea.nextLine();

        System.out.print("Введите вид упоковки: ");
        this.kind_of_packing = tea.nextLine();

        System.out.print("Введите стоимость: ");
        this.cost = tea.nextDouble();

        //tea.close();
    }

    @Override
    public void delete() {

        this.kind_of_packing = null;
        this.productID = null;
        this.company_supplier = null;
        this.country_manufacturer = null;
        this.cost = 0;
        this.name = null;

        Coffe.counter--;

    }

    @Override
    public void read() {
        System.out.println("ID товара:\t" + this.productID);

        System.out.println("Название:\t" + this.name);

        System.out.println("Страна производитель:\t" + this.country_manufacturer);

        System.out.println("Фирма поставщик:\t" + this.company_supplier);

        System.out.println("Цена:\t" + this.cost);

        System.out.println("Вид упоковки:\t" + this.kind_of_packing);

    }

    private String kind_of_packing; //Вид упоковки
}
