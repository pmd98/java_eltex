package com.company;

import java.util.Date;
import java.util.Scanner;

public class Order implements ICrudAction {

    @Override
    public void read() {
        System.out.println("\nДанные пользователя:\n");
        credentials.read();
        System.out.println("\nКорзина:\n");
        shoppingCart.read();
        System.out.println("Время создания:" + time_of_creature);
        System.out.println("Время ожидания:" + time_of_waiting);
        System.out.print("Статус: ");
        if (this.is_complete())
            System.out.println("\"Выполнен\"");
        else
            System.out.println("\"В ожидании\"");
    }

    public Order() {
        this.time_of_creature = null;
        this.time_of_waiting = null;
        this.shoppingCart = new ShoppingCart();
        this.credentials = new Credentials();
        this.order_status = false;
    }

    public Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.shoppingCart = new ShoppingCart();
        this.credentials = new Credentials();
        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.time_of_creature = new Date();
        this.time_of_waiting = new Date(time_of_creature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.order_status = false;

    }

    public void create_Order(ShoppingCart shoppingCart, Credentials credentials) {
        this.credentials = credentials;
        this.shoppingCart = shoppingCart;
        this.time_of_creature = new Date();
        this.time_of_waiting = new Date(time_of_creature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.order_status = false;
    }

    @Override
    public void create() {
        this.credentials.create();
        this.shoppingCart.create();
        this.time_of_creature = new Date();
        this.time_of_waiting = new Date(time_of_creature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.order_status = false;
    }

    @Override
    public void update() {
        this.credentials.update();
        this.shoppingCart.update();
        this.time_of_creature = new Date();
        this.time_of_waiting = new Date(time_of_creature.getTime() + (long) (((Math.random() * 10) % 5 + 5) * 1000));
        this.order_status = false;
    }

    @Override
    public void delete() {
        this.credentials.delete();
        this.shoppingCart.delete();
    }

    public boolean is_complete() {
        Date date = new Date();
        if (time_of_waiting.getTime() <= date.getTime())
            order_status = true;
        else
            order_status = false;
        return order_status;
    }

    public Date getTime_of_creature() {
        return this.time_of_creature;
    }

    public Date getTime_of_waiting() {
        return this.time_of_waiting;
    }

    private boolean order_status;
    private Date time_of_creature;
    private Date time_of_waiting;
    private ShoppingCart shoppingCart;
    private Credentials credentials;
}