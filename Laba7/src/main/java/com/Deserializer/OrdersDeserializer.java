package com.Deserializer;

import com.Orders.*;
import java.lang.reflect.Type;
import java.util.Map;
import com.google.gson.*;

public class OrdersDeserializer implements JsonDeserializer<Orders> {
    @Override
    public Orders deserialize(JsonElement element, Type type, JsonDeserializationContext context)
            throws JsonParseException {

        Orders orders = new Orders();
        JsonObject jsonObject = element.getAsJsonObject();

        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            Order order = context.deserialize(entry.getValue(), Order.class);
            orders.add(order);
        }
        return orders;
    }
}