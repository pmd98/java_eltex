package com.company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.UUID;

public class ShoppingCart implements ICrudAction {

    ShoppingCart()
    {
        list=new ArrayList<Product>();
        list_id = new HashSet<UUID>();
    }

    public boolean add(Product data)
    {

        boolean check =  list_id.add(data.getProductID());

        if(check)
        {
            list.add(data);
            return true;
        }

        else
        {
            return false;
        }
    }

    public Product delete(int index)
    {
        Product data;
        data=list.remove(index);
        return data;
    }

    public Product get(int index)
    {
        return list.get(index);
    }

    @Override
    public void create() {

        int random_number = (int)(Math.random()*10%3+3);

        for (int i=0; i < random_number ;++i)
        {
            Coffe coffe = new Coffe();
            coffe.create();
            Tea tea = new Tea();
            tea.create();
            list.add(coffe);
            list.add(tea);
            list_id.add(coffe.getProductID());
            list_id.add(tea.getProductID());
        }
    }

    @Override
    public void update()
    {
        Scanner vybor = new Scanner(System.in);

        System.out.println("Ввод продуктов в корзине");
        System.out.println("Ввод количества продукта [кофе] в корзине:");
        int b;
        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);


        for (int i=0; i < b;++i)
        {
            Coffe coffe = new Coffe();
            System.out.println("\nВвод данных в объект №"+(i+1)+":");
            coffe.update();
            list.add(coffe);
            list_id.add(coffe.getProductID());
        }
        System.out.println("Ввод количества продукта [чай] в корзине:");

        do {
            b = vybor.nextInt();
            if (b >= 0) {
                break;
            }
            System.out.println(" Некорректный ввод! ");
        } while (true);


        for (int i=0; i < b;++i)
        {
            Tea tea = new Tea();
            System.out.println("\nВвод данных в объект №"+(i+1)+":");
            tea.update();
            list.add(tea);
            list_id.add(tea.getProductID());
        }
    }

    @Override
    public void delete() {

        for (int i=0; i < list.size();++i)
        {
            list.get(i).delete();
        }
        list.clear();
        list_id.clear();
    }

    @Override
    public void read()
    {

        for (int i=0; i < list.size();++i)
        {
            list.get(i).read();
        }
    }

    public long size()
    {
        return list.size();
    }

    public boolean search_by_ID(UUID id)
    {
        return  list_id.contains(id);
    }

    private ArrayList<Product> list;
    private HashSet<UUID> list_id;


}
