package com.company.Deserializer;

import com.company.Orders.Coffe;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class CoffeDeserializer implements JsonDeserializer<Coffe> {

    @Override
    public Coffe deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Coffe coffe = new Coffe();

        coffe.setKindOfCoffeeBeans(jsonObject.get("Вид кофейных зерен").getAsString());
        coffe.setProductID(jsonObject.get("ID товара").getAsString().toString());
        coffe.setCost(jsonObject.get("Цена").getAsDouble());
        coffe.setCompanySupplier(jsonObject.get("Фирма поставщик").getAsString());
        coffe.setCountryManufacturer(jsonObject.get("Страна производитель").getAsString());
        coffe.setName(jsonObject.get("Название").getAsString());

        return coffe;
    }


}