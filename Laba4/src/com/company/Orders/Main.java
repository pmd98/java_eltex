package com.company.Orders;

import com.company.ACheck.ACheck;
import com.company.ACheck.ACheckCreator;
import com.company.ACheck.ACheckFalse;
import com.company.ACheck.ACheckTrue;


import java.util.Scanner;

public class Main {


    private static void menu() {
        System.out.println("\n               МЕНЮ ");
        System.out.println(" 1 - Вывод");
        System.out.println(" 2 - Добавление");
        System.out.println(" 3 - Удаление");
        System.out.println(" 4 - Обновление");
        System.out.println(" 5 - Вывод счетчика продуктов");
        System.out.println(" 0 - Выход");
    }

    public static void main(String[] args) {


        final  int VALUE =10;
        int var = 1;

        Orders orders = new Orders();

        ACheck<Order> checkTrue;
        ACheck<Order> checkFalse;
        ACheck<Order> checkCreate;

        for (int i = 0; i < VALUE; ++i) {
            checkCreate = new ACheckCreator<>(orders);
            checkTrue = new ACheckTrue<>(orders);
            checkFalse = new ACheckFalse<>(orders);
            orders.read();


            try {
                checkCreate.getThread().join();
                checkTrue.getThread().join();
                checkFalse.getThread().join();
                Thread.sleep(5000  );
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            orders.read();
            System.out.println("------------------------------------------------------------");
            System.out.println("------------------------------------------------------------");
        }

    }

}
